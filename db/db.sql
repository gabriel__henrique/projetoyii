-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.24-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para ap_controle
DROP DATABASE IF EXISTS `ap_controle`;
CREATE DATABASE IF NOT EXISTS `ap_controle` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `ap_controle`;

-- Copiando estrutura para tabela ap_controle.administradora
DROP TABLE IF EXISTS `administradora`;
CREATE TABLE IF NOT EXISTS `administradora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeAdm` varchar(255) NOT NULL DEFAULT '',
  `cnpj` varchar(14) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.administradora: ~15 rows (aproximadamente)
/*!40000 ALTER TABLE `administradora` DISABLE KEYS */;
INSERT INTO `administradora` (`id`, `nomeAdm`, `cnpj`, `dataCadastro`) VALUES
	(1, 'Escola', '11222333011111', '2022-03-30 12:59:55'),
	(2, 'Proway', '99555666000177', '2022-03-30 13:01:48'),
	(3, 'error', '65456654654564', '2022-05-09 21:40:43'),
	(6, 'cccc', 'dssa', '2022-04-01 11:07:07'),
	(8, 'caneta', '232323232', '2022-05-09 21:51:59'),
	(10, 'professor', '00123654000188', '2022-04-11 10:21:57'),
	(11, 'professor', '00123654000188', '2022-04-11 10:22:08'),
	(12, 'quadro', '00000000000100', '2022-04-11 13:22:09'),
	(13, 'insomnia', '03214658000589', '2022-04-14 13:09:49'),
	(14, 'testeW', '03214658000589', '2022-04-14 13:35:39'),
	(15, 'testeW2', '03214658000589', '2022-04-14 13:37:55'),
	(16, 'testando', '03214658000589', '2022-04-14 14:28:14'),
	(19, 'Teste Jquery', '01123321000112', '2022-04-20 08:53:41'),
	(20, 'teste dados', '00123654000188', '2022-04-20 09:09:19'),
	(21, 'teste vies', '123', '2022-04-20 09:57:46'),
	(23, 'televisao', '78979879879879', '2022-05-09 21:49:10');
/*!40000 ALTER TABLE `administradora` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.bloco
DROP TABLE IF EXISTS `bloco`;
CREATE TABLE IF NOT EXISTS `bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCondominio` int(11) NOT NULL DEFAULT 0,
  `nomeBloco` varchar(255) NOT NULL DEFAULT '',
  `qtAndar` int(11) NOT NULL DEFAULT 0,
  `qtAptoAndar` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `chBlocoCondominio` (`idCondominio`),
  CONSTRAINT `chBlocoCondominio` FOREIGN KEY (`idCondominio`) REFERENCES `condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela ap_controle.bloco: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `bloco` DISABLE KEYS */;
INSERT INTO `bloco` (`id`, `idCondominio`, `nomeBloco`, `qtAndar`, `qtAptoAndar`) VALUES
	(27, 64, 'carteira', 7, 8),
	(28, 64, 'cadeira', 8, 8),
	(29, 67, 'computador', 9, 9),
	(30, 67, 'tela', 7, 7),
	(31, 66, 'chaveiro', 7, 7),
	(32, 66, 'teztJquery', 5, 5),
	(33, 65, 'curso', 8, 8),
	(34, 65, 'sala', 8, 8);
/*!40000 ALTER TABLE `bloco` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.condominio
DROP TABLE IF EXISTS `condominio`;
CREATE TABLE IF NOT EXISTS `condominio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idAdm` int(11) NOT NULL DEFAULT 0,
  `nomeCondominio` varchar(255) NOT NULL DEFAULT '',
  `qtBlocos` int(11) NOT NULL DEFAULT 0,
  `cep` varchar(8) NOT NULL DEFAULT '0',
  `logradouro` varchar(255) NOT NULL DEFAULT '',
  `numero` varchar(8) NOT NULL DEFAULT '0',
  `bairro` varchar(255) NOT NULL DEFAULT '',
  `cidade` varchar(255) NOT NULL DEFAULT '',
  `estado` varchar(2) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chCondominioADM` (`idAdm`),
  CONSTRAINT `chCondominioADM` FOREIGN KEY (`idAdm`) REFERENCES `administradora` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.condominio: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `condominio` DISABLE KEYS */;
INSERT INTO `condominio` (`id`, `idAdm`, `nomeCondominio`, `qtBlocos`, `cep`, `logradouro`, `numero`, `bairro`, `cidade`, `estado`, `dataCadastro`) VALUES
	(64, 3, 'TesteInsomnia23', 5, '85480654', 'rua nao sei', '456', 'cafundo', 'judas', 'AC', '2022-04-22 15:55:04'),
	(65, 3, 'proway', 9, '85480654', 'rua tifa', '2280', 'encano', 'indaial', '', '2022-04-13 11:17:55'),
	(66, 8, 'mochila', 7, '65485790', 'rua 7 setembro', '1600', 'centro', 'blumenau', '', '2022-04-13 11:19:06'),
	(67, 3, 'maquina', 9, '89086525', 'Rua Tifa Pausch', '123', 'Encano', 'Indaial', 'SC', '2022-05-09 21:42:18'),
	(68, 12, 'teste', 4, '85480700', 'rua alamade weed', 'dasdasda', 'cone', 'crew', 'SC', '2022-04-14 08:09:47'),
	(72, 23, 'controle', 9, '89086525', 'Rua Tifa Pausch', '8', 'Encano', 'Indaial', 'SC', '2022-05-09 21:50:05'),
	(73, 23, 'pilha', 7, '89080242', 'Rua Rio Branco', '9', 'Tapajós', 'Indaial', 'SC', '2022-05-09 21:50:55');
/*!40000 ALTER TABLE `condominio` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.conselho
DROP TABLE IF EXISTS `conselho`;
CREATE TABLE IF NOT EXISTS `conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCondominio` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `funcao` enum('sindico','subsindico','conselheiro') NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `chConselhoCondominio` (`idCondominio`),
  CONSTRAINT `chConselhoCondominio` FOREIGN KEY (`idCondominio`) REFERENCES `condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela ap_controle.conselho: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `conselho` DISABLE KEYS */;
INSERT INTO `conselho` (`id`, `idCondominio`, `nome`, `funcao`) VALUES
	(91, 64, 'zezin', 'subsindico');
/*!40000 ALTER TABLE `conselho` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.morador
DROP TABLE IF EXISTS `morador`;
CREATE TABLE IF NOT EXISTS `morador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCondominio` int(11) NOT NULL DEFAULT 0,
  `idBloco` int(11) NOT NULL DEFAULT 0,
  `idUnidade` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL DEFAULT '0',
  `telefone` varchar(20) DEFAULT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `chMoradorBloco` (`idBloco`),
  KEY `chMoradorCondominio` (`idCondominio`),
  KEY `chMoradorUnidade` (`idUnidade`),
  CONSTRAINT `chMoradorBloco` FOREIGN KEY (`idBloco`) REFERENCES `bloco` (`id`),
  CONSTRAINT `chMoradorCondominio` FOREIGN KEY (`idCondominio`) REFERENCES `condominio` (`id`),
  CONSTRAINT `chMoradorUnidade` FOREIGN KEY (`idUnidade`) REFERENCES `unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela ap_controle.morador: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `morador` DISABLE KEYS */;
INSERT INTO `morador` (`id`, `idCondominio`, `idBloco`, `idUnidade`, `nome`, `cpf`, `email`, `telefone`, `dataCadastro`) VALUES
	(1, 64, 28, 17, 'zezin', '000000000', 'gabriel.hamazonas@gmail.com', '47991594765', '2022-05-09 21:48:41'),
	(4, 67, 29, 22, 'Carlinha', '78979878989', 'gabriel.hamazonas@gmail.com', '47991594765', '2022-05-09 21:48:20'),
	(5, 66, 31, 26, 'lucas', '78954621389', 'gabriel.hamazonas@gmail.com', '47986548996', '2022-05-09 21:48:33'),
	(6, 66, 32, 28, 'zezin', '78798797987', 'gabriel.hamazonas@gmail.com', '4798565', '2022-05-09 21:48:01'),
	(10, 67, 30, 37, 'testeJquert', '01236547858', 'gabriel.hamazonas@gmail.com', '47991594765', '2022-04-22 13:46:02'),
	(11, 67, 30, 24, 'testeApi', '12345678985', 'gabriel.hamazonas@gmail.com', '47991594765', '2022-04-22 13:50:21'),
	(12, 67, 29, 19, 'Gabriel', '10064510999', 'gabriel.hamazonas@gmail.com', '47991594765', '2022-05-05 22:27:30'),
	(13, 67, 29, 19, 'Hyago', '14799846546', 'hyago_roberto@hotmail.com', '47991356547', '2022-05-09 21:51:37');
/*!40000 ALTER TABLE `morador` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.unidade
DROP TABLE IF EXISTS `unidade`;
CREATE TABLE IF NOT EXISTS `unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCondominio` int(11) NOT NULL DEFAULT 0,
  `idBloco` int(11) NOT NULL DEFAULT 0,
  `numero` int(11) NOT NULL DEFAULT 0,
  `metragem` int(11) NOT NULL DEFAULT 0,
  `qtVagas` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `chUnidadeBloco` (`idBloco`),
  KEY `chUnidadeCondominio` (`idCondominio`),
  CONSTRAINT `chUnidadeBloco` FOREIGN KEY (`idBloco`) REFERENCES `bloco` (`id`),
  CONSTRAINT `chUnidadeCondominio` FOREIGN KEY (`idCondominio`) REFERENCES `condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela ap_controle.unidade: ~21 rows (aproximadamente)
/*!40000 ALTER TABLE `unidade` DISABLE KEYS */;
INSERT INTO `unidade` (`id`, `idCondominio`, `idBloco`, `numero`, `metragem`, `qtVagas`) VALUES
	(17, 64, 28, 202, 15, 15),
	(18, 64, 28, 303, 32, 32),
	(19, 67, 29, 252, 23, 32),
	(20, 64, 27, 555, 35, 3),
	(21, 64, 27, 312, 25, 25),
	(22, 67, 29, 654, 56, 56),
	(23, 67, 30, 302, 302, 320),
	(24, 67, 30, 65, 6565, 65),
	(25, 66, 31, 555, 36, 3),
	(26, 66, 31, 36, 363, 6),
	(27, 66, 32, 89, 89, 89),
	(28, 66, 32, 555, 35, 3),
	(29, 66, 32, 32, 32, 32),
	(30, 65, 33, 23, 23, 32),
	(31, 65, 34, 32, 32, 32),
	(32, 65, 34, 101, 35, 3),
	(33, 65, 33, 466, 23, 32),
	(34, 65, 33, 32, 32, 32),
	(35, 64, 27, 0, 35, 3),
	(36, 64, 27, 0, 35, 3),
	(37, 67, 30, 215646, 35, 3);
/*!40000 ALTER TABLE `unidade` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.usuarios
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT '0',
  `usuario` varchar(255) DEFAULT '0',
  `senha` varchar(255) DEFAULT '0',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.usuarios: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `nome`, `usuario`, `senha`, `dataCadastro`) VALUES
	(22, 'Gabriel', 'Gabriel', '1234', '2022-04-18 08:55:47');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.veiculo
DROP TABLE IF EXISTS `veiculo`;
CREATE TABLE IF NOT EXISTS `veiculo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCondominio` int(11) NOT NULL,
  `idMorador` int(11) NOT NULL,
  `placa` varchar(50) NOT NULL DEFAULT '',
  `marca` varchar(50) NOT NULL DEFAULT '',
  `modelo` varchar(50) NOT NULL DEFAULT '',
  `cor` varchar(50) NOT NULL DEFAULT '',
  `vaga` varchar(50) NOT NULL DEFAULT '',
  `tipoVeiculo` enum('moto','carro') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ch_Veiculo_Condominio` (`idCondominio`),
  KEY `ch_Veiculo_Morador` (`idMorador`),
  CONSTRAINT `ch_Veiculo_Condominio` FOREIGN KEY (`idCondominio`) REFERENCES `condominio` (`id`),
  CONSTRAINT `ch_Veiculo_Morador` FOREIGN KEY (`idMorador`) REFERENCES `morador` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.veiculo: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `veiculo` DISABLE KEYS */;
INSERT INTO `veiculo` (`id`, `idCondominio`, `idMorador`, `placa`, `marca`, `modelo`, `cor`, `vaga`, `tipoVeiculo`) VALUES
	(2, 67, 4, 'C84A54', 'Agrale', 'MARRUÁ AM 100 2.8  CS TDI Diesel', 'azul balde', '101', 'carro');
/*!40000 ALTER TABLE `veiculo` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
