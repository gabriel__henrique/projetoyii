<?
namespace app\modules\api\controllers;

use app\models\BlocoModel;
use app\models\CondominioModel;
use app\models\MoradorModel;
use app\models\UnidadeModel;
use Exception;
use yii\web\Controller;


class MoradorController extends Controller{
    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }
    
    public function actionGetAll(){
        $query = (new \yii\db\Query())
        ->select('mor.id,
        mor.idCondominio,
        mor.idBloco,
        mor.idUnidade,
        cond.nomeCondominio,
        b.nomeBloco,
        unid.numero,
        mor.nome,
        mor.cpf,
        mor.genero,
        mor.email,
        mor.telefone,
        mor.dataCadastro
        ')
        ->from(MoradorModel::tableName().' mor')
        ->innerJoin(CondominioModel::tableName().' cond','cond.id = mor.idCondominio')
        ->innerJoin(BlocoModel::tableName().' b', 'b.id = mor.idBloco')
        ->innerJoin(UnidadeModel::tableName().' unid','unid.id = mor.idUnidade');
        $data = $query->orderBy('nome')->all();
        $dados = [];
        $i = 0;

        if($query->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResult'] = $query->count();
            foreach($data as $d){
                foreach($d as $ch=>$value){
                    $dados['resultSet'][$i][$ch] = $value;
                }
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existem dados';
        }
        return json_encode($dados);

    }
    public function actionGetOne(){
        $request = \yii::$app->request;
        $query = (new \yii\db\Query())
        ->select('mor.id,
        mor.idCondominio,
        mor.idBloco,
        mor.idUnidade,
        cond.nomeCondominio,
        b.nomeBloco,
        unid.numero,
        mor.nome,
        mor.cpf,
        mor.genero,
        mor.email,
        mor.telefone,
        mor.dataCadastro
        ')
        ->from(MoradorModel::tableName().' mor')
        ->innerJoin(CondominioModel::tableName().' cond','cond.id = mor.idCondominio')
        ->innerJoin(BlocoModel::tableName().' b', 'b.id = mor.idBloco')
        ->innerJoin(UnidadeModel::tableName().' unid','unid.id = mor.idUnidade');
        $d = $query->where(['id' => $request->get('id')])->one();

        if($query->count() > 0 ){
            $dados['endPoint']['status'] = 'success';
            foreach($d as $ch=>$value){
                $dados['resultSet'][$ch] = $value;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existem dados';
        }
        return json_encode($dados);
    }

    public function actionRegisterMorador(){
        $request = \yii::$app->request;
        try{
            if($request->isPost){
                $model = new MoradorModel();
                $model->attributes = $request->post();
                $model->save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido';
                return json_encode($dados);
            }
        }catch(Exception $th){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro nao pode ser inserido';
            return json_encode($dados);
        }
    }

    public function actionEditMorador(){
        $request = \yii::$app->request;
        try{
            if($request->isPost){
                $model = MoradorModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();
                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro atualizado com sucesso';
                return json_encode($dados);
            }
        }catch(Exception $th){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro nao pode ser atualizado';
            return json_encode($dados);
        }
    }

    public function actionDeleteMorador(){
        $request = \yii::$app->request;
        try{
            if($request->isPost){
                $model = MoradorModel::findOne($request->post('id'));
                $model->delete();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro deletado';
                return json_encode($dados);
            }
        }catch(Exception $th){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não pode ser deletado';
            return json_encode($dados);
        }
    }
}
?>