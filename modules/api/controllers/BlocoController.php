<?
namespace app\modules\api\controllers;

use app\models\BlocoModel;
use app\models\CondominioModel;
use Exception;
use yii\web\Controller;


class BlocoController extends Controller{
    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }
    
    public function actionGetAll(){
        $query = (new \yii\db\Query())
        ->select('
        b.id,
        b.idCondominio,
        b.nomeBloco,
        b.qtAndar,
        b.qtAptoAndar,
        cond.nomeCondominio
        ')
        ->from(BlocoModel::tableName().' b')
        ->innerJoin(CondominioModel::tableName().' cond', 'cond.id = b.idCondominio');
        $data = $query->orderBy('nomeBloco')->all();
        $dados = [];
        $i = 0;

        if($query->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResult'] = $query->count();
            foreach($data as $d){
                foreach($d as $ch=>$value){
                    $dados['resultSet'][$i][$ch] = $value;
                }
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existem dados';
        }
        return json_encode($dados);

    }
    public function actionGetOne(){
        $request = \yii::$app->request;
        $query = (new \yii\db\Query())
        ->select('
        b.id,
        b.idCondominio,
        b.nomeBloco,
        b.qtAndar,
        b.qtAptoAndar,
        cond.nomeCondominio
        ')
        ->from(BlocoModel::tableName().' b')
        ->innerJoin(CondominioModel::tableName().' cond', 'cond.id = b.idCondominio');
        $d = $query->where(['b.id' => $request->get('id')])->one();

        if($query->count() > 0 ){
            $dados['endPoint']['status'] = 'success';
            foreach($d as $ch=>$value){
                $dados['resultSet'][$ch] = $value;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existem dados';
        }
        return json_encode($dados);
    }

    public function actionGetAllFromCondo(){
        $request = \yii::$app->request;
        $query = BlocoModel::find();
        $data = $query->where(['idCondominio' => $request->get('idCondominio')])->all();
        $dados = [];

        if($query->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResult'] = $query->count();
            $i = 0;
            foreach($data as $ch=>$d){
                $dados['resultSet'][$i]['id'] = $d['id'];
                $dados['resultSet'][$i]['nomeBloco'] = $d['nomeBloco'];
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existem dados';
        }
        return json_encode($dados);

    }

    public function actionRegisterBloco(){
        $request = \yii::$app->request;
        try{
            if($request->isPost){
                $model = new BlocoModel();
                $model->attributes = $request->post();
                $model->save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido';
                return json_encode($dados);
            }
        }catch(Exception $th){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro nao pode ser inserido';
            return json_encode($dados);
        }
    }

    public function actionEditBloco(){
        $request = \yii::$app->request;
        try{
            if($request->isPost){
                $model = BlocoModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();
                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro atualizado com sucesso';
                return json_encode($dados);
            }
        }catch(Exception $th){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro nao pode ser atualizado';
            return json_encode($dados);
        }
    }

    public function actionDeleteBloco(){
        $request = \yii::$app->request;
        try{
            if($request->isPost){
                $model = BlocoModel::findOne($request->post('id'));
                $model->delete();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro deletado';
                return json_encode($dados);
            }
        }catch(Exception $th){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não pode ser deletado';
            return json_encode($dados);
        }
    }
}
?>