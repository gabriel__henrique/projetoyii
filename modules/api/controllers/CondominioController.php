<?
namespace app\modules\api\controllers;

use app\models\AdministradoraModel;
use app\models\CondominioModel;
use Exception;
use yii\web\Controller;

class CondominioController extends Controller{
    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }
    
    public function actionGetAll(){
        $qry = (new \yii\db\Query())
        ->select('cond.id,
        cond.idAdm,
        adm.nomeAdm,
        cond.nomeCondominio,
        cond.qtBlocos,
        cond.cep,
        cond.logradouro,
        cond.numero,
        cond.bairro,
        cond.cidade,
        cond.estado,
        cond.dataCadastro')
        ->from(CondominioModel::tableName().' cond')
        ->innerJoin(AdministradoraModel::tableName().' adm','adm.id = cond.idAdm');
        $data = $qry->orderBy('nomeCondominio')->all();
        $dados = [];
        $i = 0;

        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach($data as $d){
                foreach($d as $ch=>$value){
                    $dados['resultSet'][$i][$ch] = $value;
                }
                $i++;
            }

        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existem dados';
        }
        return json_encode($dados);
    }

    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = (new \yii\db\Query())
        ->select('cond.id,
        cond.idAdm as idADM,
        adm.nomeAdm,
        cond.nomeCondominio,
        cond.qtBlocos,
        cond.cep,
        cond.logradouro,
        cond.numero,
        cond.bairro,
        cond.cidade,
        cond.estado,
        cond.dataCadastro')
        ->from(CondominioModel::tableName().' cond')
        ->innerJoin(AdministradoraModel::tableName().' adm','adm.id = cond.idAdm');
        $d = $qry->where(['cond.id' => $request->get('id')])->one();

        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            foreach($d as $ch=>$value)
            $dados['resultSet'][0][$ch] = $value;
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existe registro';
        }
        return json_encode($dados);
    }

    public function actionRegisterCond(){
        $request = \yii::$app->request;
        try{
            if($request->isPost){
                $model = new CondominioModel();
                $model->attributes = $request->post();
                $model->save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido com sucesso.';
                return json_encode($dados);
            }
        }catch(Exception $th){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao pode ser inserido';
        }
    }

    public function actionEditCond(){
        $request = \yii::$app->request;
        try{
            if($request->isPost){
                $model = CondominioModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro atualizado com sucesso';
                return json_encode($dados);
            }
        }catch(Exception $th){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro nao pode ser atualizado';
            return json_encode($dados);
        }
    }

    public function actionDeleteCond(){
        $request = \yii::$app->request;
        try{
            if($request->isPost){
                $model = CondominioModel::findOne($request->post('id'));
                $model->delete();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro deletado';
                return json_encode($dados);
            }
        }catch(Exception $th){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não pode ser deletado';
            return json_encode($dados);
        }
    }
}
?>