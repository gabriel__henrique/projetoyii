<?
namespace app\models;

use Yii;


/**
 * LoginForm is the model bahind the login form
*/
class LoginForm extends User{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    /**
     * @return array the validation rules
     */
    public function rules(){
        return [
            [['usuario', 'usuario'], 'required'],
            ['senha', 'validatePassword'],
        ];
    }

    /**
     * validates the password.
     * this method serves as the inline validation for password.
     * 
     * @param string $attribute the attribute currently being validated.
     * @param array $params the additional name-value pairs given in the rules
     */
    public function validatePassword($attribute, $params){
        if(!$this->hasErrors()){
            $user = $this->getUser();

            if(!$user || !$user->validatePassword($this->password)){
                $this->addError($attribute, 'Dados de login não correspondem.');
            }
        }
    }

    public function login (){
        if ($this->validate()){
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * finds user by [[username]]
     * 
     * @return User|null
     */
    public function getUser(){
        if ($this->_user === false){
            $this->_user = User::findOne(['username' => $this->username]);
        }
        return $this->_user;
    }
}
?>