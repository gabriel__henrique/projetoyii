<?
namespace app\models;
use yii\db\ActiveRecord;

class ConselhoModel extends ActiveRecord{
    public static function tableName()
    {
        return 'conselho';
    }

    public function rules(){
        return [
            [['idCondominio','nome','funcao'],'required']
        ];
    }
}
?>