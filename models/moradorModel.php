<?
namespace app\models;
use yii\db\ActiveRecord;

class MoradorModel extends ActiveRecord{
    public static function tableName(){
        return 'morador';
    }

    public function rules(){
        return [
            [['idCondominio','idBloco','idUnidade','nome','cpf','email','telefone'], 'required']
        ];
    }
}
?>