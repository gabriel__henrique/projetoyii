<?
namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface{

    public static function tableName(){
        return 'usuarios';
    }

    /** 
    *Este método procura por um instancia da classe de identidae usando o ID de usuario especificado.
    *Este método é usado qinado voce precisa manter o status de login via sessão 
    *@param [type] $id
    *@return void
    */
    public static function findIdentity($id){
        return static::findOne($id);
    } 
    /**
     * ele procura por uma instancia da classe de identidade usando o token de acesso informado.
     * este método é usado quando precisa autenticar um usuario por um unico token sereto (exemplo: em uma aplicacao stateless RESTful)
     * 
     * @param [type] $token
     * @param [type] $type
     * @return void
     */
    public static function findIdentityByAccessToken($token, $type = null){
        return null;//static::findOne(['token' => $token])
    }

     /** 
     * retorna o ID do usuario representado por essa instância da classe de identidade
     * 
     * @return void
     */
    public function getId(){
        return $this->id;
    }
    /**
     * retorna uma chave para verificar login via cookie. A chave é mantida no cookie de login e sera comparada com o informação do lado servidor para testar a vakudade do cookie
     * 
     * @return void
     */
    public function getAuthKey()
    {
        return null;//$this->auth_key;
    }
    /**
     * implementa a lógica de verificação da chave de login via cookie;
     * 
     * @param [type] $authKey
     * @return void
     */
    public function validateAuthKey($authKey){
        return null;//$this->auth_key == $authKey;
    }
}
?>