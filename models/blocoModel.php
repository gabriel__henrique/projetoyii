<?
namespace app\models;
use yii\db\ActiveRecord;

class BlocoModel extends ActiveRecord{
    public static function tableName(){
        return 'bloco';
    }

    public function rules(){
        return [
        [['idCondominio','nomeBloco','qtAndar','qtAptoAndar'],'required']
        ];
    }
}
?>