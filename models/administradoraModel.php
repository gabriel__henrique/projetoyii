<?
namespace app\models;
use yii\db\ActiveRecord;

class AdministradoraModel extends ActiveRecord{
    public static function tableName(){
        return 'administradora';    
    }

    //método para validar dados
    // public function rules(){
    //     return[
    //         [['nomeAdm','nomeAdm'], 'required'],
    //         [['cnpj','cnpj'], 'required'],
    //     ];
    // }
    public function rules(){
        return [
            [['nomeAdm', 'cnpj'], 'required'],
        ];
    }
}
?>