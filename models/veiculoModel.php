<?
namespace app\models;

use yii\db\ActiveRecord;

class VeiculoModel extends ActiveRecord{

    public static function tableName(){
        return 'veiculo';
    }

    public function rules(){
        return [
            [['idCondominio','idMorador', 'marca', 'placa','modelo','cor','vaga', 'tipoVeiculo'], 'required']
        ];
    }
}
?>