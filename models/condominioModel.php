<?
namespace app\models;
use yii\db\ActiveRecord;
global $query;
class CondominioModel extends ActiveRecord{
    public static function tableName(){
        return 'condominio';
    }

    public function rules(){
        return[
            [['idAdm','nomeCondominio','qtBlocos','cep','logradouro','numero','bairro','cidade','estado'],'required']
        ];
    }
}
?>