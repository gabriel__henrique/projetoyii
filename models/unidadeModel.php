<?
namespace app\models;
use yii\db\ActiveRecord;

class UnidadeModel extends ActiveRecord{
    public static function tableName(){
        return 'unidade';
    }

    public function rules(){
        return [
            [['idCondominio','idBloco','numero','metragem','qtVagas'], 'required']
        ];
    }
}
?>