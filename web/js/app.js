$(function(){

    //mascaras
    function masks(){
        $(document).ready(function(){
            $('input[name="cpf"]').mask('000.000.000-00');

            var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };
        
            $('input[name="telefone"]').mask(SPMaskBehavior, spOptions);
        
        
            $('input[name="cep"]').mask('00000-000');
        
            $('input[name="cnpj"]').mask('00.000.000/0000-00');
        })
    }
    masks()
     

    function modalbody(caminho){
        return new Promise((resolve) => {
            $(".modal-body").load(caminho, function (response, status){
                console.log('teste')
                if(status === "success"){
                    $('#modalComponent').modal({show: true})
                    resolve(true);
                }
            })
        })
    }
    $('.openModal').click(function(){
        let janela = $(this).attr('href');
        modalbody(janela).then((data =>{
            if(data){
                masks()
                let tipo = $('.tipoVeiculo').find('option:selected').attr('value');
                console.log(tipo);
                selectMarca(tipo).then((data =>{
                    if(data){
                        let marca = $('.selectMarca').find('option:selected').attr('data-id')
                        selectModelo(tipo, marca)
                    }
                }))
            }
        }))
        return false
    })

    $(document).on('change','.actionGenero',function(){
        var gen = $(this).val();
        var name = $(this).attr('id');
        if(gen == 'O'){
            $(this).attr('name','');
            $(this).parent().append('<input class="form-control HelicopteroDeAtaque" type="text" name="'+name+'">');
        }else{
            $('.HelicopteroDeAtaque').remove();
            $(this).attr('name',name)
            
        }
    })

    $(document).on('change','.fromCondominio',function(){
        selecionado = $(this).val();

        $.ajax({
            url: '?r=bloco/lista-bloco-api',
            dataType: 'json',
            type: 'POST',
            data: {id: selecionado},
            success : function(data){
                selectPopulation('.fromBloco',data, 'nomeBloco')
            }
        })
    })
    $(document).on('change','.fromBloco',function(){
        selecionado = $(this).val();

        $.ajax({
            url: '?r=unidade/lista-unidade-api',
            dataType: 'json',
            type: 'POST',
            data: {id: selecionado},
            success : function(data){
                selectPopulation('.fromUnidade',data, 'numero' )
            }
        })
    })

    $(document).on('change','.MorfromCondominio',function(){
        selecionado = $(this).val();

        $.ajax({
            url: '?r=morador/lista-morador-api',
            dataType: 'json',
            type: 'POST',
            data: {id: selecionado},
            success : function(data){
                selectPopulation('.fromMorador',data, 'nome')
            }
        })
    })
    
    //api para marca
    function selectMarca(tipo){
        return new Promise((resolve, reject) => {
            let options = `<option>Selecione uma marca</option>`
            let urlApi = ''
            if(tipo == 'moto'){
                urlApi = `https://parallelum.com.br/fipe/api/v1/motos/marcas`
            }else{
                urlApi = `https://parallelum.com.br/fipe/api/v1/carros/marcas`
            }
            $.ajax({
                url: urlApi,
                method: 'GET',
                dataType: 'json',
                success: function(data){
                    console.log(data)
                    let dados = data
                    let dataItem = $('.selectMarca').attr('data-item'); 
                    for(let key in dados){
                        options += `<option data-id="${dados[key].codigo}" ${(dataItem == dados[key].nome)? 'selected' : ''} value="${dados[key].nome}">${dados[key].nome}</option>`
                    }
                    $('.selectMarca').html(options)
                    resolve(true)
                },
                error: function(data){
                    reject(false)
                }
            })
        })
    }
    //autocomplete dos select
    $(document).on('change','.tipoVeiculo', function(){
       let tipo = $(this).val();
       selectMarca(tipo)
    })

    //api do modelo
    function selectModelo(tipo, marca){
        let options = `<option value="">Selecione...</option>`
        let urlApi = '';
        if(tipo == 'moto'){
            urlApi = `https://parallelum.com.br/fipe/api/v1/motos/marcas/${marca}/modelos`
        }else{
            urlApi = `https://parallelum.com.br/fipe/api/v1/carros/marcas/${marca}/modelos`
        }
        $.ajax({
            url: urlApi,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                console.log(data)
                let dados = data.modelos
                let modeloSelecionado = $('.selectModelo').attr('data-item'); 
                for(let key in dados){
                    options += `<option  value="${dados[key].nome}" ${(modeloSelecionado == dados[key].nome)? 'selected' : ''}>${dados[key].nome}</option>`
                }
                $('.selectModelo').html(options)
            }
        })
    }
    //aucomplete dos select de modelo
    $(document).on('change','.selectMarca',function(){
        let marca = $(this).find('option:selected').attr('data-id')
       
        let tipo = $(this).parent().parent().find('.tipoVeiculo').val()
        
        selectModelo(tipo, marca)
    })

    function selectPopulation(seletor, dados, field){
        estrutura = '<option value="">Selecione...</option>';


            for (let i = 0; i < dados.length; i++){
                estrutura += '<option value="'+dados[i].id+'">'+dados[i][field]+'</option>'
            }


        $(seletor).html(estrutura);
    }

    //autocomplemento
    autocomplete = function(cep){

        $.ajax({
            url: `https://viacep.com.br/ws/${cep}/json/`,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                console.log(data);
                $('.logradouro').val(data.logradouro)
                $('.bairro').val(data.bairro)
                $('.cidades').val(data.localidade)
                $('.fromEstados').val(data.uf)               
            }
        })
    }
    //autocomplemento pelo cep
    $(document).on('keyup','.cep',function(){
        let cep = $(this).val();
        if(cep.length >= 8){
            autocomplete(cep)
        }
        
    })
    
    //popular cidade com base nos estados
    $(document).on('change','.fromEstados',function(){
        let options = `<option value="">Selecione...</option>`
        let estado = $(this).val()
        let estadoSelecionado = $(this).find(`option[value="${estado}"]`).attr('data-uf')
        $.ajax({
            url: `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${estadoSelecionado}/municipios`,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                for(let key in data){
                    options += `<option value="${data[key].nome}">${data[key].nome}</option>`
                }
                $('.cidades').html(options)
            }
        })
    })

    //popular select do cep (caso nao saiba)
    $(document).on('keyup','.logradouroColapse',function(){
        let options = `<option value="">Selecione...</option>`
        let logradouro = $(this).val()
        let estado = $(this).parent().parent().find('.fromEstados').val()
        let cidade = $(this).parent().parent().find('.cidades').val()
        let estadoSelecionado = $(this).parent().parent().find(`option[value="${estado}"]`).val()
        if(logradouro.length >= 3){
            $.ajax({
                url: `https://viacep.com.br/ws/${estadoSelecionado}/${cidade}/${logradouro}/json/`,
                method: 'GET',
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    for(let key in data){
                        options += `<option value="${data[key].cep}">${data[key].cep} ${data[key].logradouro} ${data[key].bairro} ${data[key].complemento}</option>`
                    }
                    $('.cepColapse').html(options)             
                }
            })
        }
    })

    //autocomplete pelo não sei meu cep
    $(document).on('change','.cepColapse',function(){
        let cep = $(this).val();
        
        $.ajax({
            url: `https://viacep.com.br/ws/${cep}/json/`,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                console.log(data);
                $('.cep').val(data.cep)      
                $('.logradouro').val(data.logradouro)
                $('.bairro').val(data.bairro)
                $('.cidades').val(data.localidade)
                $('.fromEstados').val(data.uf)      
            }
        })
        $('#collapseExample').removeClass('show')
        $('.mascaraPreta').hide()
    })

    $(document).on('click','.naoseiCep',function(){
        $('.mascaraPreta').show()
    })
})

function myAlert(tipo, mensagem, pai, url){
    url = (url == undefined) ? url = '' : url = url;
      componente ='<div class="alert alert-'+tipo+'" role="alert">'+mensagem+'</div>';
      
      $(pai).prepend(componente);

      setTimeout(function(){
          $(pai).find('div.alert').remove();
          //redirecionar?
          if(tipo == 'success' && url){ 
              setTimeout(function(){
                  window.location.href = url;
              },500)
          }
      }, 3000)
  }
