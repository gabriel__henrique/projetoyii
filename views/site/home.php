<?

use app\controllers\AdministradoraController;
use app\controllers\CondominioController;
use app\controllers\MoradorController;
use yii\helpers\Html;

?>
<script
src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js">
</script>
<div class="row">
    <canvas id="myChart" style="width:100%;max-width:700px"></canvas>
</div>

<div class="row">
    <div class="col-md-6">
        <table class="table table-dark table-striped">
            <tr>
                <td scope="col" class="text-center">Administradora</td>
                <td scope="col" class="text-center">total Condominio</td>
            </tr>
            <?
            foreach(AdministradoraController::listaContobyAdm() as $dados){
            ?>
            <tr>
                <td class="text-center"><?=$dados['nomeAdm']?></td>
                <td class="text-center"><?=$dados['totalcond']?></td>
            </tr>
            <? }?>
        </table>
    </div>
    <div class="col-md-6">

        <table class="table table-dark table-striped">
            <tr>
                <td scope="col" class="text-center">Condominio</td>
                <td scope="col" class="text-center">Total moradores</td>
            </tr>
            <?
            foreach(CondominioController::listamorBycondo() as $dados){
            ?>
            <tr>
                <td class="text-center"><?=$dados['nomeCondominio']?></td>
                <td class="text-center"><?=$dados['totalmor']?></td>
            </tr>
            <? }?>
        </table>
    </div>
</div>






























<script>
var xValues = ["Condominio", "Morador", "Administradora"];
var yValues = [<?=CondominioController::totalcond()?>, <?=MoradorController::totalmor()?>, <?=AdministradoraController::totaladm()?>, 0];
var barColors = ["rgba(10,24,118,1)", "indigo","rgba(64,14,12,1)"];

new Chart("myChart", {
  type: "bar",
  data: {
    labels: xValues,
    datasets: [{
      backgroundColor: barColors,
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    title: {
      display: true,
      text: "Total de Registros"
    }
  }
});
</script>
