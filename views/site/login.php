<?

use app\components\alertComponent;
use yii\helpers\Url;

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/signin.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Document</title>
</head>

<body class="text-center">

    <main class="container mt-5">
        <form action="<?=Url::to(['site/login'])?>" class="form-signin" method="POST">
        <?
        if(isset($_GET['myAlert'])){

            echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg'],$_GET['myAlert']['redir']);
            }
        ?>
            <h1 class="h3 mb-3 font-weight-normal">Faça login</h1>
            <label for="inputEmail" class="sr-only">Email</label>
            <input type="text" id="inputEmail" class="form-control" name="usuario" placeholder="Usuario" required autofocus>
            <label for="inputPassword" class="sr-only">Senha</label>
            <input type="password" id="inputPassword" class="form-control" name="senha" placeholder="Senha" required>
            <div class=" mb-3">
                    <input type="hidden" name="<?=\yii::$app->request->csrfParam?>" value="<?=\yii::$app->request->csrfToken?>">
            </div>
            <button class="btn btn-lg bg-dark text-light btn-block" type="submit">Logar</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2017-2021</p>
        </form>
        
        <script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        
</main>
</body>

</html>

<script src="js/jquery-3.6.0.min.js"></script>