<?

use app\components\generoComponent;
use app\components\modalComponent;
use app\controllers\CondominioController;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\helpers\Url;
?>
<div class="row">
    <h1 class="col-12 col-sm12 text-center mb-5">Cadastro De Pessoas</h1>
</div>

<form action="<?=Url::to(['morador/realiza-cadastro-morador'])?>" class="form-morador my-5" method="POST">
    <div class="form-row">
    <div class="form-group col-md-3">
      <select id="inputState" class="fromCondominio form-control" name="idCondominio">
          <option selected>condominio</option>
          <?foreach(CondominioController::listaCondominioSelect() as $condo){?>
            <option value="<?=$condo['id']?>"><?=$condo['nomeCondominio']?></option>
          <?}?>
        </select>
      </div>
    <div class="form-group col-md-3">
        <select id="inputState" class="fromBloco form-control" name="idBloco">
       
        </select>
      </div>
      <div class="form-group col-md-3">
        <select id="inputState" class="fromUnidade form-control" name="idUnidade">
        
        </select>
      </div>
    </div>
    <div class="form-group">
        <input class="form-control" type="text" name="nome" placeholder="Nome" value="" required>
    </div>
    <div class="form-group">
        <input class="form-control" type="text" name="cpf" placeholder="CPF" value="" required>
    </div>
    
    <div class="form-group">
        <input class="form-control" type="email" name="email" placeholder="E-mail" value="" required>
    </div>
    <div class="form-group">
        <input class="form-control" type="text" name="telefone" placeholder="Telefone" value="">
    </div>
    
    <div class="form-group">
            <input type="hidden" name="<?=\yii::$app->request->csrfParam; ?>" value="<?=\yii::$app->request->csrfToken;?>">
    </div>
    <button class="btn btn-primary col-12 col-sm-3 buttonEnviar" type="submit">Enviar</button>
</form>


