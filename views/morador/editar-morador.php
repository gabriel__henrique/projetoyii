<?

use app\components\generoComponent;
use app\components\modalComponent;
use app\components\selectedComponent;
use app\controllers\BlocoController;
use app\controllers\CondominioController;
use app\controllers\UnidadeController;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\helpers\Url;
?>
<form action="<?=Url::to(['morador/realiza-edicao-morador'])?>" class="form-morador my-5" method="POST">
    <div class="form-row">
    <div class="form-group col-md-3">
      <select id="inputState" class="fromCondominio form-control" name="idCondominio">
          <option selected>condominio</option>
          <?foreach(CondominioController::listaCondominioSelect() as $condo){?>
            <option value="<?=$condo['id']?>" <?=selectedComponent::isSelected($condo['id'],$edit['idCondominio'])?>><?=$condo['nomeCondominio']?></option>
          <?}?>
        </select>
      </div>
    <div class="form-group col-md-3">
        <select id="inputState" class="fromBloco form-control" name="idBloco">
            <option value="">Selecione...</option>
            
        <?foreach(BlocoController::listaBlocoSelectEdit($edit['idCondominio']) as $bloco){?>
            <option value="<?=$bloco['id']?>" <?=selectedComponent::isSelected($bloco['id'],$edit['idBloco'])?>><?=$bloco['nomeBloco']?></option>
          <?}?>
        </select>
      </div>
      <div class="form-group col-md-3">
        <select id="inputState" class="fromUnidade form-control" name="idUnidade">
        <option value="">Selecione...</option>
        <?foreach(UnidadeController::listaUnidadeSelectEdit($edit['idBloco']) as $unidade){?>
            <option value="<?=$unidade['id']?>" <?=selectedComponent::isSelected($unidade['id'],$edit['idUnidade'])?>><?=$unidade['numero']?></option>
          <?}?>
        </select>
      </div>
    </div>
    <div class="form-group">
        <input class="form-control" type="text" name="nome" placeholder="Nome" value="<?=$edit['nome']?>" required>
    </div>
    <div class="form-group">
        <input class="form-control" type="text" name="cpf" placeholder="CPF" value="<?=$edit['cpf']?>" required>
    </div>
  
    <div class="form-group">
        <input class="form-control" type="email" name="email" placeholder="E-mail" value="<?=$edit['email']?>" required>
    </div>
    <div class="form-group">
        <input class="form-control" type="text" name="telefone" placeholder="Telefone" value="<?=$edit['telefone']?>">
    </div>
    <div class="form-group">
    </div>
    <div class="form-group">
            <input type="hidden" name="id" value="<?=$edit['id']?>">
            <input type="hidden" name="<?=\yii::$app->request->csrfParam; ?>" value="<?=\yii::$app->request->csrfToken;?>">
    </div>
    <button class="btn btn-primary buttonEnviar" type="submit">Salvar</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
</form>


