<?

use app\components\selectedComponent;
use app\controllers\CondominioController;
use yii\helpers\Url;
?>
<form action="<?=Url::to(['conselho/realiza-edicao-conselho'])?>" class="formConselho" method="POST">
  <div class="form-row">
  <div class="form-group col-md-3">
        <select id="inputState" class="form-control" name="idCondominio">
          <option selected>condominio</option>
          <?foreach(CondominioController::listaCondominioSelect() as $condo){?>
            <option value="<?=$condo['id']?>" <?=selectedComponent::isSelected($condo['id'],$edit['idCondominio'])?>><?=$condo['nomeCondominio']?></option>
          <?}?>
        </select>
    </div>
    <div class="form-group col-md-2">
        <input type="text" class="form-control" name="nome" value="<?=$edit['nome']?>" placeholder="nome">
    </div>
    <div class="form-group col-md-3">
        <select id="inputState" class="form-control" name="funcao">
          <option selected>função</option>
          <option value="sindico">sindico</option>
          <option value="subsindico">subSindico</option>
          <option value="conselheiro">conselheiro</option>
        </select>
    </div>
  <div class="form-group">
    <input type="hidden" name="id" value="<?=$edit['id']?>">
    <input type="hidden" name="<?=\yii::$app->request->csrfParam; ?>" value="<?=\yii::$app->request->csrfToken;?>">
  </div>
  <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
</form>