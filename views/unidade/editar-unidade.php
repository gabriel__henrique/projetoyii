<?

use app\components\selectedComponent;
use app\controllers\BlocoController;
use app\controllers\CondominioController;
use yii\helpers\Url;
?>

<form action="<?=Url::to(['unidade/realiza-edicao-unidade'])?>" class="formUnidade" method="POST">
  <div class="form-row">
  <div class="form-group col-md-3">
        <select id="inputState" class="fromCondominio form-control" name="idCondominio">
          <option>Selecione...</option>
        <?foreach(CondominioController::listaCondominioSelect() as $condo){?>
          <option value="<?=$condo['id']?>" <?=selectedComponent::isSelected($condo['id'],$edit['idCondominio'])?>><?=$condo['nomeCondominio']?></option>
          <?}?>
        </select>
      </div>
    <div class="form-group col-md-3">
    <select id="inputState" class="fromBloco form-control" name="idBloco">
            <option value="">Selecione...</option>
            
        <?foreach(BlocoController::listaBlocoSelectEdit($edit['idCondominio']) as $bloco){?>
            <option value="<?=$bloco['id']?>" <?=selectedComponent::isSelected($bloco['id'],$edit['idBloco'])?>><?=$bloco['nomeBloco']?></option>
          <?}?>
        </select>
      </div>
      <div class="form-group col-md-2">
        <input type="text" class="form-control" name="numero" value="<?=$edit['numero']?>" placeholder="numero">
      </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="metragem" value="<?=$edit['metragem']?>" placeholder="metragem">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="qtVagas" value="<?=$edit['qtVagas']?>" placeholder="Vagas Garagem">
    </div>
  </div>
  <div class="form-group">
    <input type="hidden" name="id" value="<?=$edit['id']?>">
    <input type="hidden" name="<?=\yii::$app->request->csrfParam; ?>" value="<?=\yii::$app->request->csrfToken;?>">
  </div>
  <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
</form>