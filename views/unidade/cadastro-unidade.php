<?
use app\controllers\CondominioController;
use yii\helpers\Url;
?>
<h1 class="text-center mb-5 text-light">Cadastro de Unidade</h1>
<form action="<?=Url::to(['unidade/realiza-cadastro-unidade'])?>" class="formUnidade" method="POST">
  <div class="form-row">
  <div class="form-group col-md-3">
        <select id="inputState" class="fromCondominio form-control" name="idCondominio">
          <option>Selecione...</option>
        <?foreach(CondominioController::listaCondominioSelect() as $condo){?>
          <option value="<?=$condo['id']?>" ><?=$condo['nomeCondominio']?></option>
          <?}?>
        </select>
      </div>
    <div class="form-group col-md-3">
        <select id="inputState" class="fromBloco form-control" name="idBloco">
        
        </select>
      </div>
      <div class="form-group col-md-2">
        <input type="text" class="form-control" name="numero" value="" placeholder="numero">
      </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="metragem" value="" placeholder="metragem">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="qtVagas" value="" placeholder="Vagas Garagem">
    </div>
  </div>
  <div class="form-group">
            <input type="hidden" name="<?=\yii::$app->request->csrfParam; ?>" value="<?=\yii::$app->request->csrfToken;?>">
    </div>
  <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
</form>