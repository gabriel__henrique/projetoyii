<?

use app\components\alertComponent;
use app\components\filtroComponent;
use app\components\modalComponent;
use app\controllers\CondominioController;
use yii\widgets\LinkPager;


global $urlSite;
if(isset($_GET['myAlert'])){

echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg'],$_GET['myAlert']['redir']);
}
?>

<h1 class="text-center">Lista de Veiculos</h1>
<table class="table table-striped table-hover table-bordered table-dark" id="listaVeiculos">
    <tr>
        <td colspan="10">
        <?=filtroComponent::filtro('veiculo','filtro-veiculo','nome','condominio',CondominioController::listaCondominioSelect(), 'nomeCondominio')?>
        </td>
    </tr>
    <tr>
        <td scope="col" class="text-center">Condominio</td>
        <td scope="col" class="text-center">Morador</td>
        <td scope="col" class="text-center">Tipo</td>
        <td scope="col" class="text-center">Marca</td>
        <td scope="col" class="text-center">Modelo</td>
        <td scope="col" class="text-center">Placa</td>
        <td scope="col" class="text-center">Cor</td>
        <td scope="col" class="text-center">vaga</td>
            
        <td class="text-center"><a href="<?=$urlSite?>?r=veiculo/cadastro-veiculo" class="btn btn-light px-3 py-0"><small class="mr-2">Adicionar</small><i class="bi bi-plus-circle"></i></a></td>
    </tr>
    <?
    foreach($veiculo as $ch2=>$dados){
    ?>
    <tr data-id="<?=$dados['id']?>">
        <td class="text-center"><?=$dados['nomeCondominio']?></td>
        <td class="text-center"><?=$dados['nome']?></td>
        <td class="text-center"><?=$dados['tipoVeiculo']?></td>
        <td class="text-center"><?=$dados['marca']?></td>
        <td class="text-center"><?=$dados['modelo']?></td>
        <td class="text-center"><?=$dados['placa']?></td>
        <td class="text-center"><?=$dados['cor']?></td>
        <td class="text-center"><?=$dados['vaga']?></td>
             
        <td class="text-center">
            <a href="<?=$urlSite?>?r=veiculo/deleta-veiculo&id=<?=$dados['id']?>" data-id="<?=$dados['id']?>" class="removerMorador text-white mr-5"><i class="bi bi-trash3"></i></a>
            <a href="<?=$urlSite?>?r=veiculo/editar-veiculo&id=<?=$dados['id']?>" class=" text-white openModal"><i class="bi bi-pencil-square"></i></a>
        </td>
    </tr>
    <? } ?>
    <tr>
        <td colspan="10" class="text-right">Total Registros <small class="badge badge-light totalRegistro"><?=$paginacao->totalCount?></small></td>
    </tr>
</table> 
<div class="row">
    <div class="col-md-12 mb-5">
        <?= LinkPager::widget(
            [
                'pagination' => $paginacao,
                'linkContainerOptions' =>[
                    'class' => 'page-item'
                ],
                'linkOptions' => [
                    'class' => 'page-link'
                ],
                'disabledListItemSubTagOptions' => [
                    'class' => 'page-link'
                ]
                ]) 
        ?>
    </div>
</div>
<?=modalComponent::modal()?>