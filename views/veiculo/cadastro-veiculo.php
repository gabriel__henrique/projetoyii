<?
use app\controllers\CondominioController;
use yii\helpers\Url;
?>
<h1 class="text-center mb-5 text-light">Cadastro de Veiculo</h1>
<form action="<?=Url::to(['veiculo/realiza-cadastro-veiculo'])?>" class="formVeiculo" method="POST">
  <div class="form-row">
  <div class="form-group col-md-3">
        <select id="inputState" class="MorfromCondominio form-control" name="idCondominio">
          <option>Selecione...</option>
        <?foreach(CondominioController::listaCondominioSelect() as $condo){?>
          <option value="<?=$condo['id']?>" ><?=$condo['nomeCondominio']?></option>
          <?}?>
        </select>
      </div>
    <div class="form-group col-md-3">
        <select id="inputState" class="fromMorador form-control" name="idMorador">
        
        </select>
      </div>
      <div class="form-group col-md-2">
      <select id="inputState" class="form-control tipoVeiculo" name="tipoVeiculo">
          <option selected>tipo</option>
          <option value="moto">moto</option>
          <option value="carro">carro</option>
        </select>
      </div>
    <div class="form-group col-md-2">
    <select id="inputState" class="selectMarca form-control" name="marca">
        
    </select>
    </div>
    <div class="form-group col-md-2">
      <select id="inputState" class="selectModelo form-control" name="modelo">
        
      </select>
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="placa" value="" placeholder="placa">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="cor" value="" placeholder="cor">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="vaga" value="" placeholder="vaga">
    </div>
  </div>
  <div class="form-group">
            <input type="hidden" name="<?=\yii::$app->request->csrfParam; ?>" value="<?=\yii::$app->request->csrfToken;?>">
    </div>
  <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
</form>