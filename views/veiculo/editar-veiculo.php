<?

use app\components\selectedComponent;
use app\controllers\CondominioController;
use app\controllers\MoradorController;
use app\controllers\VeiculoController;
use yii\helpers\Url;
?>

<form action="<?=Url::to(['veiculo/realiza-edicao-veiculo'])?>" class="formVeiculo" method="POST">
  <div class="form-row">
  <div class="form-group col-md-3">
        <select id="inputState" class="MorfromCondominio form-control" name="idCondominio">
          <option>Selecione...</option>
        <?foreach(CondominioController::listaCondominioSelect() as $condo){?>
          <option value="<?=$condo['id']?>" <?=selectedComponent::isSelected($condo['id'], $edit['idCondominio'])?>><?=$condo['nomeCondominio']?></option>
          <?}?>
        </select>
      </div>
    <div class="form-group col-md-3">
        <select id="inputState" class="fromMorador form-control" name="idMorador">
        <option value="">Selecione...</option>
            
        <?foreach(MoradorController::listaMoradorSelectEdit($edit['idCondominio']) as $mor){?>
            <option value="<?=$mor['id']?>" <?=selectedComponent::isSelected($mor['id'],$edit['idMorador'])?>><?=$mor['nome']?></option>
          <?}?>
        </select>
      </div>
      <div class="form-group col-md-2">
      <select id="inputState" class="form-control tipoVeiculo" name="tipoVeiculo">
          <option>tipo</option>
          <option value="moto" <?=selectedComponent::isSelected('moto', $edit['tipoVeiculo'])?>>moto</option>
          <option value="carro"<?=selectedComponent::isSelected('carro', $edit['tipoVeiculo'])?>>carro</option>
        </select>
      </div>
    <div class="form-group col-md-2">
    <select id="inputState" class="selectMarca form-control" name="marca" data-item="<?=$edit['marca']?>">

    </select>
    </div>
    <div class="form-group col-md-2">
      <select id="inputState" class="selectModelo form-control" name="modelo" data-item="<?=$edit['modelo']?>">
      
      </select>
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="placa" value="<?=$edit['placa']?>" placeholder="placa">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="cor" value="<?=$edit['cor']?>" placeholder="cor">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="vaga" value="<?=$edit['vaga']?>" placeholder="vaga">
    </div>
  </div>
  <div class="form-group">
    <input type="hidden" name="id" value="<?=$edit['id']?>">
            <input type="hidden" name="<?=\yii::$app->request->csrfParam; ?>" value="<?=\yii::$app->request->csrfToken;?>">
    </div>
  <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
</form>