<?

use app\components\alertComponent;
use app\components\maskComponent;
use app\components\modalComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;

global $urlSite;
if(isset($_GET['myAlert'])){

    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}
?>
<h1 class="text-center">Lista Administradora</h1>

<table class="table table-striped table-hover table-bordered table-dark" id="listaAdm">
    <tr>
            <td colspan="10">
            <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
                <input type="hidden" name="r" value="administradora/filtro-administradora">
                <input class="form-control mr-sm-2 termo1" type="search" placeholder="busca por nome" aria-label="Search" name="termo1">
                <button class="btn btn-outline-primary my-2 my-sm-0" type="submit" >Buscar</button>
                <a class="btn btn-outline-danger my-2 my-sm-0 ml-3" href="<?=$urlSite?>?r=administradora/lista-administradora">Limpar</a>
            </form>
            </td>
        </tr>
        <tr>
            <td scope="col" class="text-center">Nome</td>
            <td scope="col" class="text-center">CNPJ</td>
            <td scope="col" class="text-center">Data</td>
            <td class="text-center"><a href="<?=$urlSite?>?r=administradora/cadastro-administradora" class="btn btn-light px-3 py-0"><small class="mr-2">Adicionar</small><i class="bi bi-plus-circle"></i></a></td>
        </tr>
        <?
        foreach($administradora as $ch2=>$dados){
        ?>
        <tr data-id="<?=$dados['id']?>">
            <td class="text-center"><?=$dados['nomeAdm']?></td>
            <td class="text-center"><?=maskComponent::mask($dados['cnpj'],'cnpj')?></td>
            <td class="text-center"><?=$dados['dataCadastro']?></td>
            <td class="text-center">
                <a href="<?=$urlSite?>?r=administradora/deleta-administradora&id=" data-id="<?=$dados['id']?>" class="removerAdm text-white mr-5"><i class="bi bi-trash3"></i></a>
                <a href="<?=$urlSite?>?r=administradora/editar-administradora&id=<?=$dados['id']?>" class=" text-white openModal"><i class="bi bi-pencil-square"></i></a>
            </td>
        </tr>
        <? } ?>
        <tr>
            <td colspan="10" class="text-right">Total Registros <small class="badge badge-light totalRegistro"><?=$paginacao->totalCount?></small></td>
        </tr>
    </table>
    <div class="row">
    <div class="col-md-12 mb-5">
        <?= LinkPager::widget(
            [
                'pagination' => $paginacao,
                'linkContainerOptions' =>[
                    'class' => 'page-item'
                ],
                'linkOptions' => [
                    'class' => 'page-link'
                ],
                'disabledListItemSubTagOptions' => [
                    'class' => 'page-link'
                ]
                ]) 
        ?>
    </div>
</div>
<?=modalComponent::modal()?>