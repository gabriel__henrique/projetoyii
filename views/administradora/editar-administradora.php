<?
use yii\helpers\Url;
?>
<form action="<?= Url::to(['administradora/realiza-edicao-administradora'])?>" class="formAdm" id="cadastroAdministradora" method="POST">
    <div class="form-row">
      <div class="form-group col-md-2">
        <input type="text" class="form-control" name="nomeAdm" value="<?=$edit['nomeAdm']?>" placeholder="Nome da Administradora" required>
      </div>
        <div class="form-group col-md-2">
      <input type="text" class="form-control" name="cnpj" value="<?=$edit['cnpj']?>" placeholder="CNPJ" required>
        </div>
    </div>
    <div class="form-group">
      <input type="hidden" name="id" value="<?=$edit['id']?>">
       <input type="hidden" name="<?=\yii::$app->request->csrfParam; ?>" value="<?=\yii::$app->request->csrfToken;?>"> 
    </div>
  <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
</form>