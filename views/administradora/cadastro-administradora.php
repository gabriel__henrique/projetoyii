<?
use yii\helpers\Url;
?>
<h1 class="text-center mb-5 text-light">Cadastro de Administradora</h1>
<form action="<?= Url::to(['administradora/realiza-cadastro-administradora'])?>" class="formAdm" id="cadastroAdministradora" method="POST">
    <div class="form-row">
      <div class="form-group col-md-2">
        <input type="text" class="form-control" name="nomeAdm" value="" placeholder="Nome da Administradora" required>
      </div>
        <div class="form-group col-md-2">
      <input type="text" class="form-control" name="cnpj" value="" placeholder="CNPJ" required>
        </div>
    </div>
    <div class="form-group">
       <input type="hidden" name="<?=\yii::$app->request->csrfParam; ?>" value="<?=\yii::$app->request->csrfToken;?>"> 
    </div>
  <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
</form>