<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;


$urlSite = Url::base(true);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => '<img src="img/Logo.png" width="100px"/>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => 'Home', 'url' =>  Yii::$app->homeUrl],
            [
                'label' => 'Listas',
                'items' =>[
                    [
                        'label' => 'Administradora',
                        'url' => ['/administradora/lista-administradora']
                    ],
                    [
                        'label' => 'Condominio',
                        'url' => ['/condominio/lista-condominio']
                    ],
                    [
                        'label' => 'Conselho',
                        'url' => ['conselho/lista-conselho']
                    ],
                    [
                        'label' => 'Bloco',
                        'url' => ['bloco/lista-bloco']
                    ],
                    [
                        'label' => 'Unidade',
                        'url' => ['unidade/lista-unidade']
                    ],
                    [
                        'label' => 'Morador',
                        'url' => ['morador/lista-morador']
                    ],
                    [
                        'label' => 'Veiculo',
                        'url' => ['veiculo/lista-veiculo']
                    ]
                ]
            ],
            [
                'label' => 'Cadastro',
                'items' =>[
                    [
                        'label' => 'Administradora',
                        'url' => ['administradora/cadastro-administradora']
                    ],
                    [
                        'label' => 'Condominio',
                        'url' => ['condominio/cadastro-condominio']
                    ],
                    [
                        'label' => 'Conselho',
                        'url' => ['conselho/cadastro-conselho']
                    ],
                    [
                        'label' => 'Bloco',
                        'url' => ['bloco/cadastro-bloco']
                    ],
                    [
                        'label' => 'Unidade',
                        'url' => ['unidade/cadastro-unidade']
                    ],
                    [
                        'label' => 'Morador',
                        'url' => ['morador/cadastro-morador']
                    ],
                    [
                        'label' => 'Veiculo',
                        'url' => ['veiculo/cadastro-veiculo']
                    ]
                ]
            ],
            Yii::$app -> user -> isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                . Html::submitButton(
                    'Logout (' . Yii::$app -> user -> identity -> usuario . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0 mt-5 container">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="container-fluid mt-auto pt-3 text-muted bg-dark">
        <div class="row">
            <div class="col-sm-4">
                <p class="float-left text-light">&copy; todos os direitos reservados <?= date('Y') ?></p>
            </div>
            <div class="col-sm-4 text-center">
                <a href="https://api.whatsapp.com/send?phone=+5547991594765"><i class="bi bi-whatsapp text-light"> Suporte</i></a>
            </div>
            <div class="col-sm-4">
                <p class="float-right text-light"><?= Yii::powered() ?></p>
            </div>
        </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
