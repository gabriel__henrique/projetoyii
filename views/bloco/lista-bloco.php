<?

use app\components\alertComponent;
use app\components\filtroComponent;
use app\components\modalComponent;
use app\controllers\CondominioController;
use yii\helpers\Html;
use yii\widgets\LinkPager;

global $urlSite;
if(isset($_GET['myAlert'])){

    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}
?>
<h1 class="text-center">Lista Bloco</h1>

<table class="table table-striped table-hover table-bordered table-dark" id="listaBloco">
    <tr>
            <td colspan="10">
            <?=filtroComponent::filtro('bloco','filtro-bloco','nome','condominio',CondominioController::listaCondominioSelect(), 'nomeCondominio')?>
            </td>
        </tr>
        <tr>
            <td scope="col" class="text-center">Condominio</td>
            <td scope="col" class="text-center">Nome Bloco</td>
            <td scope="col" class="text-center">qt Andar</td>
            <td scope="col" class="text-center">Apto por Andar</td>
            <td class="text-center"><a href="<?=$urlSite?>?r=bloco/cadastro-bloco" class="btn btn-light px-3 py-0"><small class="mr-2">Adicionar</small><i class="bi bi-plus-circle"></i></a></td>
        </tr>
        <?
        foreach($bloco as $ch2=>$dados){
        ?>
        <tr data-id="<?=$dados['id']?>">
            <td class="text-center"><?=$dados['nomeCondominio']?></td>
            <td class="text-center"><?=$dados['nomeBloco']?></td>
            <td class="text-center"><?=$dados['qtAndar']?></td>
            <td class="text-center"><?=$dados['qtAptoAndar']?></td>
            <td class="text-center">
                <a href="<?=$urlSite?>?r=bloco/deleta-bloco&id=<?=$dados['id']?>" data-id="<?=$dados['id']?>" class="removerBloco text-white mr-5"><i class="bi bi-trash3"></i></a>
                <a href="<?=$urlSite?>?r=bloco/editar-bloco&id=<?=$dados['id']?>" class=" text-white openModal"><i class="bi bi-pencil-square"></i></a>
            </td>
        </tr>
        <? } ?>
        <tr>
            <td colspan="10" class="text-right">Total Registros <small class="badge badge-light totalRegistro"><?=$paginacao->totalCount?></small></td>
        </tr>
     </table>
     <div class="row">
    <div class="col-md-12 mb-5">
        <?= LinkPager::widget(
            [
                'pagination' => $paginacao,
                'linkContainerOptions' =>[
                    'class' => 'page-item'
                ],
                'linkOptions' => [
                    'class' => 'page-link'
                ],
                'disabledListItemSubTagOptions' => [
                    'class' => 'page-link'
                ]
                ]) 
        ?>
    </div>
</div>
<?=modalComponent::modal()?>