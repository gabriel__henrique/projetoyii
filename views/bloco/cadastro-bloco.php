<?

use app\controllers\CondominioController;;
use yii\helpers\Url;
?>
<h1 class="text-center mb-5 text-light">Cadastro de Bloco</h1>
<form action="<?=Url::to(['bloco/realiza-cadastro-bloco'])?>" class="formBloco" method="POST">
  <div class="form-row">
    <div class="form-group col-md-3">
        <select id="inputState" class="form-control" name="idCondominio">
          <option value="">selecionar</option>
          <?foreach(CondominioController::listaCondominioSelect() as $condo){?>
          <option value="<?=$condo['id']?>"><?=$condo['nomeCondominio']?></option>
          <?}?>
        </select>
      </div>
      <div class="form-group col-md-2">
        <input type="text" class="form-control" name="nomeBloco" value="" placeholder="Nome do Bloco">
      </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="qtAndar" value="" placeholder="Quantidade Andar">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="qtAptoAndar" value="" placeholder="Apto por Andar">
    </div>
  </div>
  <div class="form-group">
        <input type="hidden" name="<?=\yii::$app->request->csrfParam; ?>" value="<?=\yii::$app->request->csrfToken;?>">
    </div>
  <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
</form>