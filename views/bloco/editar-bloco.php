<?

use app\components\selectedComponent;
use app\controllers\CondominioController;;
use yii\helpers\Url;
?>
<form action="<?=Url::to(['bloco/realiza-edicao-bloco'])?>" class="formBloco" method="POST">
  <div class="form-row">
    <div class="form-group col-md-3">
        <select id="inputState" class="form-control" name="idCondominio">
          <option value="">selecionar</option>
          <?foreach(CondominioController::listaCondominioSelect() as $condo){?>
          <option value="<?=$condo['id']?>" <?=selectedComponent::isSelected($condo['id'],$edit['idCondominio'])?>><?=$condo['nomeCondominio']?></option>
          <?}?>
        </select>
      </div>
      <div class="form-group col-md-2">
        <input type="text" class="form-control" name="nomeBloco" value="<?=$edit['nomeBloco']?>" placeholder="Nome do Bloco">
      </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="qtAndar" value="<?=$edit['qtAndar']?>" placeholder="Quantidade Andar">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="qtAptoAndar" value="<?=$edit['qtAptoAndar']?>" placeholder="Apto por Andar">
    </div>
  </div>
  <div class="form-group">
    <input type="hidden" name="id" value="<?=$edit['id']?>">
    <input type="hidden" name="<?=\yii::$app->request->csrfParam; ?>" value="<?=\yii::$app->request->csrfToken;?>">
  </div>
  <button class="btn btn-primary buttonEnviar" type="submit">Salvar</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
</form>