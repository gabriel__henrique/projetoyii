<?

use app\components\estadosComponent;
use app\controllers\AdministradoraController;
use yii\helpers\Url;
?>

<h1 class="text-center mb-5">Cadastro de Condominio</h1>
<div class="collapse" id="collapseExample">
    <div class="card bg-cep card-body" style="z-index: 999;">
        <div class="row">
            <div class="col-12 col-md-6 mb-3">
                <label for="estado">Estado</label>
                <select id="inputState" class="form-control fromEstados">
                    <option value="">selecione...</option>
                    <?foreach(estadosComponent::estados() as $sig=>$uf){?>
                        <option value="<?=$uf?>" data-uf="<?=$sig?>"><?=$uf?></option>
                    <?}?>
                </select> 
            </div>   
            <div class="col-12 col-md-6 mb-3">
                <label for="cidade">Cidade</label>
                <select id="inputState" class="form-control cidades">
                    <option value="">Carregando...</option>
                </select>
            </div> 
            <div class="col-12 col-md-12 mb-12">
                <label for="logradouroColapse">Logradouro</label>
                <input type="text" class="form-control logradouroColapse" id="logradouroColapse" placeholder="Logradouro">
            </div> 
            <div class="col-12 col-md-12 mb-3">
                <label for="inputState">CEP</label>
                <select id="inputState" class="form-control cepColapse">
                    <option value="">Carregando...</option>
                </select>
            </div>     
        </div>
    </div>
</div>
<form action="<?=Url::to(['condominio/realiza-cadastro-condominio'])?>" class="formCondominio" method="POST">
  <div class="form-row">
    <div class="form-group col-md-3">
          <select id="inputState" class="form-control" name="idAdm" required>
            <option value="">selecionar</option>
            <?
            foreach(AdministradoraController::listaAdministradoraSelect() as $adm){?>
            <option value="<?=$adm['id']?>"><?=$adm['nomeAdm']?></option>
            <? } ?>
          </select>
    </div>
    <div class="form-group col-md-5">
      <input type="text" class="form-control" name="nomeCondominio" value="" placeholder="Nome do condominio" required>
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="qtBlocos" value="" placeholder="Quantidade Blocos" required>
    </div>
  </div>
  <div class="row">
    <div class="form-group col-md-2">
      <input type="text" class="form-control cep" name="cep" value="" placeholder="CEP" required>
      <a class="naoseiCep" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
          Não sei meu CEP<i class="icofont-google-map"></i>
      </a>
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control logradouro" name="logradouro" value="" placeholder="Logradouro" required>
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="numero" value=""  placeholder="Numero" required>
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control bairro" name="bairro" value="" placeholder="Bairro" required>
    </div>
</div>

<div class="form-row">
</div>

  <div class="form-row">


    <div class="form-group col-md-2">
        <select id="inputState" class="form-control fromEstados" name="estado" required>
          <option value="">selecionar</option>
          <?
          foreach(estadosComponent::estados() as $sig=>$uf){?>
          <option data-uf="<?=$sig?>" value="<?=$uf?>"><?=$uf?></option>
          <? } ?>
        </select>
      </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control cidades" name="cidade" value="" placeholder="Cidade" required>
    </div>
  </div>
  <div class="form-group">
            <input type="hidden" name="<?=\yii::$app->request->csrfParam; ?>" value="<?=\yii::$app->request->csrfToken;?>">
    </div>
  <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
</form>
<div class="mascaraPreta">

</div>