<?

use app\components\alertComponent;
use app\components\filtroComponent;
use app\components\maskComponent;
use app\components\modalComponent;
use app\controllers\AdministradoraController;
use yii\helpers\Html;
use yii\widgets\LinkPager;

global $urlSite;

if(isset($_GET['myAlert'])){

    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}
?>
<h1 class="text-center">Lista Condominio</h1>

<table class="table table-striped table-hover table-bordered table-dark" id="listaCondominio">
    <tr>
            <td colspan="10">
            <?=filtroComponent::filtro('condominio','filtro-condominio','nome','administradora',AdministradoraController::listaAdministradoraSelect(),'nomeAdm')?>
            </td>
        </tr>
        <tr>
            <td scope="col" class="text-center">Administradora</td>
            <td scope="col" class="text-center">Nome</td>
            <td scope="col" class="text-center">qt.Blocos</td>
            <td scope="col" class="text-center">CEP</td>
            <td scope="col" class="text-center">Logradouro</td>
            <td scope="col" class="text-center">N°</td>
            <td scope="col" class="text-center">Bairro</td>
            <td scope="col" class="text-center">Cidade</td>
            <td scope="col" class="text-center">Estado</td>

            <td class="text-center"><a href="<?=$urlSite?>?r=condominio/cadastro-condominio" class="btn btn-light px-3 py-0"><small class="mr-2">Adicionar</small><i class="bi bi-plus-circle"></i></a></td>
        </tr>
        <?
        foreach($condominio as $dados){
        ?>
        <tr data-id="<?=$dados['id']?>">
            <td class="text-center"><?=$dados['nomeAdm']?></td>
            <td class="text-center"><?=$dados['nomeCondominio']?></td>
            <td class="text-center"><?=$dados['qtBlocos']?></td>
            <td class="text-center"><?=maskComponent::mask($dados['cep'],'cep')?></td>
            <td class="text-center"><?=$dados['logradouro']?></td>
            <td class="text-center"><?=$dados['numero']?></td>
            <td class="text-center"><?=$dados['bairro']?></td>
            <td class="text-center"><?=$dados['cidade']?></td>
            <td class="text-center"><?=$dados['estado']?></td>
            <td class="text-center">
                <a href="<?=$urlSite?>?r=condominio/deleta-condominio&id=<?=$dados['id']?>" data-id="<?=$dados['id']?>" class="removerCondominio text-white mr-5"><i class="bi bi-trash3"></i></a>
                <a href="<?=$urlSite?>?r=condominio/editar-condominio&id=<?=$dados['id']?>" class=" text-white openModal"><i class="bi bi-pencil-square"></i></a>
            </td>
        </tr>
        <? }?>
        <tr>
            <td colspan="10" class="text-right">Total Registros <small class="badge badge-light totalRegistro"><?=$paginacao->totalCount?></small></td>
        </tr>
     </table>
     <div class="row">
    <div class="col-md-12 mb-5">
        <?= LinkPager::widget(
            [
                'pagination' => $paginacao,
                'linkContainerOptions' =>[
                    'class' => 'page-item'
                ],
                'linkOptions' => [
                    'class' => 'page-link'
                ],
                'disabledListItemSubTagOptions' => [
                    'class' => 'page-link'
                ]
                ]) 
        ?>
    </div>
</div>
<?=modalComponent::modal()?>