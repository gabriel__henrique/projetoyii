<?

use app\components\estadosComponent;
use app\components\selectedComponent;
use app\controllers\AdministradoraController;
use yii\helpers\Url;
?>
<div class="collapse" id="collapseExample">
    <div class="card bg-cep card-body" style="z-index: 999;">
        <div class="row">
            <div class="col-12 col-md-6 mb-3">
                <label for="estado">Estado</label>
                <select id="inputState" class="form-control fromEstados">
                    <option value="">selecione...</option>
                    <?foreach(estadosComponent::estados() as $sig=>$uf){?>
                        <option value="<?=$uf?>" data-uf="<?=$sig?>"><?=$uf?></option>
                    <?}?>
                </select> 
            </div>   
            <div class="col-12 col-md-6 mb-3">
                <label for="cidade">Cidade</label>
                <select id="inputState" class="form-control cidades">
                    <option value="">Carregando...</option>
                </select>
            </div> 
            <div class="col-12 col-md-12 mb-12">
                <label for="logradouroColapse">Logradouro</label>
                <input type="text" class="form-control logradouroColapse" id="logradouroColapse" placeholder="Logradouro">
            </div> 
            <div class="col-12 col-md-12 mb-3">
                <label for="inputState">CEP</label>
                <select id="inputState" class="form-control cepColapse">
                    <option value="">Carregando...</option>
                </select>
            </div>     
        </div>
    </div>
</div>
<form action="<?=Url::to(['condominio/realiza-edicao-condominio'])?>" class="formCondominio" method="POST">
  <div class="form-row">
  <div class="form-group col-md-3">
        <select id="inputState" class="form-control" name="idAdm" required>
          <option value="">selecionar</option>
          <?
          foreach(AdministradoraController::listaAdministradoraSelect() as $adm){?>
          <option value="<?=$adm['id']?>" <?=selectedComponent::isSelected($adm['id'],$edit['idAdm'])?>><?=$adm['nomeAdm']?></option>
          <? } ?>
        </select>
      </div>
    <div class="form-group col-md-5">
      <input type="text" class="form-control" name="nomeCondominio" value="<?=$edit['nomeCondominio']?>" placeholder="Nome do condominio" required>
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="qtBlocos" value="<?=$edit['qtBlocos']?>" placeholder="Quantidade Blocos" required>
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control cep" name="cep" value="<?=$edit['cep']?>" placeholder="CEP" required>
      <a class="naoseiCep" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
          Não sei meu CEP<i class="icofont-google-map"></i>
      </a>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-10">
      <input type="text" class="form-control logradouro" name="logradouro" value="<?=$edit['logradouro']?>" placeholder="Logradouro" required>
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="numero" value="<?=$edit['numero']?>"  placeholder="Numero" required>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <input type="text" class="form-control bairro" name="bairro" value="<?=$edit['bairro']?>" placeholder="Bairro" required>
    </div>
    

    <div class="form-group col-md-4">
      <input type="text" class="form-control cidades" name="cidade" value="<?=$edit['cidade']?>" placeholder="Cidade" required>
    </div>
    <div class="form-group col-md-3">
        <select id="inputState" class="form-control fromEstados" name="estado" required>
          <option value="">selecionar</option>
          <?
          foreach(estadosComponent::estados() as $sig=>$uf){?>
          <option data-uf="<?=$sig?>" value="<?=$uf?>" <?=selectedComponent::isSelected($uf,$edit['estado'])?>><?=$uf?></option>
          <? } ?>
        </select>
      </div>
  </div>
  <div class="form-group">
    <input type="hidden" name="id" value="<?=$edit['id']?>">  
    <input type="hidden" name="<?=\yii::$app->request->csrfParam; ?>" value="<?=\yii::$app->request->csrfToken;?>">
  </div>
    <button class="btn btn-primary buttonEnviar" type="submit">Salvar</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
</form>
<div class="mascaraPreta">

</div>