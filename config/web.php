<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'name' => 'ApControle',
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'hLWMLOWFWj3QD55hx_JBWjbXFGuX3Tax',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        'formatter' =>[
            'dateFormat' => 'dd/MM/yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator'=> '.',
            'currencyCode'=> 'R$',
            'nullDisplay'=>'Sem data'
        ],
        'legivelComponent' =>[
            'class' => 'app\components\legivelComponent'
        ],
        'estadosComponent' => [
            'class' => 'app\components\estadosComponent'
        ],
        'generoComponent' => [
            'class' => 'app\components\generoComponent'
        ],
        'maskComponent' =>[
            'class' => 'app\components\maskComponent'
        ],
        'alertComponent' =>[
            'class' => 'app\components\alertComponent'
        ],
        'generoComponent' => [
            'class' => 'app\components\generoComponent'
        ],
        'modalComponent' =>[
            'class' => 'app\components\modalComponent'
        ],
        'selectedComponent' =>[
            'class' => 'app\components\selectedComponent'
        ],
        'filtroComponent' => [
            'class' => 'app\components\filtroComponent'
        ]
    ],
    'params' => $params,
    'modules' =>[
        'api' =>[
            'class' => 'app\modules\api\Module'
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
