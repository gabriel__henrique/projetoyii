<?
namespace app\controllers;

use app\models\CondominioModel;
use app\models\MoradorModel;
use app\models\VeiculoModel;
use Exception;
use yii\web\Controller;
use yii\data\Pagination;
use Yii;
use yii\db\Command;

class VeiculoController extends Controller{
    public function actionListaVeiculo(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }


        $query = (new \yii\db\Query())
        ->select('veic.id,
        veic.idCondominio,
        veic.idMorador,
        cond.nomeCondominio,
        mor.nome,
        veic.placa,
        veic.marca,
        veic.modelo,
        veic.cor,
        veic.vaga,
        veic.tipoVeiculo
        ')
        ->from(VeiculoModel::tableName().' veic')
        ->innerJoin(CondominioModel::tableName().' cond','cond.id = veic.idCondominio')
        ->innerJoin(MoradorModel::tableName().' mor','mor.id = veic.idMorador');



        $paginacao = new Pagination([
            'defaultPageSize'=> 5,
            'totalCount'=> $query->count(),
        ]);

        $veiculo = $query->orderBy('marca')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();

        return $this->render('lista-veiculo', [
            'veiculo'=> $veiculo,
            'paginacao'=>$paginacao,
        ]);
    }

    public function actionFiltroVeiculo(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $request = \yii::$app->request;

        $query = (new \yii\db\Query())
        ->select('veic.id,
        veic.idCondominio,
        veic.idMorador,
        cond.nomeCondominio,
        mor.nome,
        veic.placa,
        veic.marca,
        veic.modelo,
        veic.cor,
        veic.vaga,
        veic.tipoVeiculo
        ')
        ->from(VeiculoModel::tableName().' veic')
        ->innerJoin(CondominioModel::tableName().' cond','cond.id = veic.idCondominio')
        ->innerJoin(MoradorModel::tableName().' mor','mor.id = veic.idMorador')
        ->where(['like', 'mor.nome', $request->get('termo1')])
        ->andWhere(['like', 'cond.nomeCondominio', $request->get('termo2')]);


        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $veiculo = $query->orderBy('nome')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();


        return $this->render('lista-veiculo',[
            'veiculo'=>$veiculo,
            'paginacao'=>$paginacao,
        ]);
    }

    public static function listaVeiculoSelect(){
        $query = VeiculoModel::find();

        return $query->orderBy('marca')->all();
    }
    public static function listaMoradorSelectEdit($from){
        $query = MoradorModel::find();
        $data = $query->where(['id' => $from])->orderBy('nome')->all();
        return $data;
    }

    public function actionCadastroVeiculo(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        return $this->render('cadastro-veiculo');
    }

    public function actionRealizaCadastroVeiculo(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new VeiculoModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['veiculo/lista-veiculo']);
        }

        return $this->render('cadastro-veiculo');
    }

    public function actionEditarVeiculo(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = VeiculoModel::find();
            $veiculo = $query->where(['id' => $request->get()])->one();
        }
        
        return $this->render('editar-veiculo',[
            'edit' => $veiculo
        ]);
    }
    public function actionRealizaEdicaoVeiculo(){
        $request = \yii::$app->request;
        
        if($request->isPost){
            $model = VeiculoModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            if($model->update()){
                return $this->redirect(['veiculo/lista-veiculo']);
            }else{
                return $this->redirect(['veiculo/lista-veiculo', 'msg' => 'erro']);
            }
        }
    }
    public function actionDeletaVeiculo(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        try {
            $request = \yii::$app->request;
            if($request->isGet){
                $model = VeiculoModel::findOne($request->get('id'));
                if($model->delete()){
                    return $this->redirect(['veiculo/lista-veiculo',
                        'myAlert' => [
                            'type' => 'success',
                            'msg' => 'Registro Deletado',
                            'redir' => 'index.php?r=veiculo/lista-veiculo'
                        ]
                    ]);
                }
                
            } 
        }catch (Exception $th) {         
            return $this->redirect(['veiculo/lista-veiculo',
            'myAlert' => [
                'type' => 'danger',
                'msg' => 'Registro não Deletado',
                'redir' => 'index.php?r=veiculo/lista-veiculo'
                ]
            ]   );
        }
    }
}
?>