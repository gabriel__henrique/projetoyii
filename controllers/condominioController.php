<?
namespace app\controllers;

use app\components\legivelComponent;
use app\components\maskComponent;
use app\models\AdministradoraModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\CondominioModel;
use Yii;

class CondominioController extends Controller{
    public function actionListaCondominio(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }



        // $query = CondominioModel::find();
        $query = (new \yii\db\Query())
        ->select('cond.id,
        cond.idAdm,
        adm.nomeAdm,
        cond.nomeCondominio,
        cond.qtBlocos,
        cond.cep,
        cond.logradouro,
        cond.numero,
        cond.bairro,
        cond.cidade,
        cond.estado,
        cond.dataCadastro')
        ->from(CondominioModel::tableName().' cond')
        ->innerJoin(AdministradoraModel::tableName().' adm','adm.id = cond.idAdm');
        

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $condominio = $query->orderBy('nomeCondominio')
        
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();

        return $this->render('lista-condominio',[
            'condominio'=>$condominio,
            'paginacao'=>$paginacao,
        ]);
    }

    public function actionFiltroCondominio(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $request = \yii::$app->request;

        $query = (new \yii\db\Query())
        ->select('
        cond.id,
        cond.idAdm,
        adm.nomeAdm,
        cond.nomeCondominio,
        cond.qtBlocos,
        cond.cep,
        cond.logradouro,
        cond.numero,
        cond.bairro,
        cond.cidade,
        cond.estado,
        cond.dataCadastro
        ')
        ->from(CondominioModel::tableName().' cond')
        ->innerJoin(AdministradoraModel::tableName().' adm', 'adm.id = cond.idAdm')
        ->where(['like', 'cond.nomeCondominio', $request->get('termo1')])
        ->andWhere(['like', 'adm.nomeAdm', $request->get('termo2')]);


        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $condominio = $query->orderBy('nomeCondominio')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();


        return $this->render('lista-condominio',[
            'condominio'=>$condominio,
            'paginacao'=>$paginacao,
        ]);
    }

    public static function listaCondominioSelect(){
        $query = CondominioModel::find();

        return $query->orderBy('nomeCondominio')->all();
    }
    public function actionCadastroCondominio(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        return $this->render('cadastro-condominio');
    }

    public function actionRealizaCadastroCondominio(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new CondominioModel();
            foreach ($request->post() as $ch => $value) {
                if($ch == 'cep'){
                    $getMani[$ch] = maskComponent::desMask($value);
                }else{
                    $getMani[$ch] = $value;
                }
            }
            $model->attributes = $getMani;
            $model->save();
            return $this->redirect(['condominio/lista-condominio']);
        }

        return $this->render('cadastro-condominio');
    }

    public function actionEditarCondominio(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = CondominioModel::find();
            $condominio = $query->where(['id' => $request->get()])->one();
        }
        
        return $this->render('editar-condominio',[
            'edit' => $condominio
        ]);
    }
    public function actionRealizaEdicaoCondominio(){
        $request = \yii::$app->request;
        
        if($request->isPost){
            $model = CondominioModel::findOne($request->post('id'));
            foreach ($request->post() as $ch => $value) {
                if($ch == 'cep'){
                    $getMani[$ch] = maskComponent::desMask($value);
                }else{
                    $getMani[$ch] = $value;
                }
            }
            $model->attributes = $getMani;
            if($model->update()){
                return $this->redirect(['condominio/lista-condominio']);
            }else{
                return $this->redirect(['condominio/lista-condominio', 'msg' => 'erro']);
            }
        }
    }
    public function actionDeletaCondominio(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        try {
            $request = \yii::$app->request;
            if($request->isGet){
                $model = CondominioModel::findOne($request->get('id'));
                if($model->delete()){
                    return $this->redirect(['condominio/lista-condominio',
                        'myAlert' => [
                            'type' => 'success',
                            'msg' => 'Registro Deletado',
                            'redir' => 'index.php?r=condominio/lista-condominio'
                        ]
                    ]);
                }
                
            } 
        }catch (\Throwable $th) {         
            return $this->redirect(['condominio/lista-condominio',
            '   myAlert' => [
                'ty pe' => 'danger',
                'msg    ' => 'Registro não Deletado',
                'redir' => 'index.php?r=condominio/lista-condominio'
                ]
            ]   );
        }
    }

    public static function totalcond(){
        $query = \yii::$app->db->createCommand('
        select
        count(id)
        from condominio
        ')->queryScalar();

        return $query;
    }

    public static function listamorBycondo(){
        $query = \yii::$app->db->createCommand('
        select
        nomeCondominio,
        count(mor.nome) as totalmor
        from condominio 
        inner join morador mor on mor.idCondominio = condominio.id
        group by nomeCondominio
        ')->queryAll();

        return $query;
    }
}
?>