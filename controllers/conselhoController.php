<?
namespace app\controllers;

use app\models\CondominioModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\ConselhoModel;
use Yii;

class ConselhoController extends Controller{
    public function actionListaConselho(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }



        $query = (new \yii\db\Query())
        ->select('con.id,
        con.idCondominio,
        cond.nomeCondominio,
        con.nome,
        con.funcao
        ')
        ->from(ConselhoModel::tableName().' con')
        ->innerJoin(CondominioModel::tableName().' cond','cond.id = con.idCondominio');

        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $conselho = $query->orderBy('nome')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();

        return $this->render('lista-conselho',[
            'conselho'=>$conselho,
            'paginacao'=>$paginacao, 
        ]);
    }

    public function actionFiltroConselho(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $request = \yii::$app->request;

        $query = (new \yii\db\Query())
        ->select('
        con.id,
        con.idCondominio,
        cond.nomeCondominio,
        con.nome,
        con.funcao
        ')
        ->from(ConselhoModel::tableName().' con')
        ->innerJoin(CondominioModel::tableName().' cond','cond.id = con.idCondominio')
        ->where(['like', 'con.nome', $request->get('termo1')])
        ->andWhere(['like', 'cond.nomeCondominio', $request->get('termo2')]);


        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $conselho = $query->orderBy('nome')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();


        return $this->render('lista-conselho',[
            'conselho'=>$conselho,
            'paginacao'=>$paginacao,
        ]);
    }

    public function actionCadastroConselho(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        return $this->render('cadastro-conselho');
    }

    public function actionRealizaCadastroConselho(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new ConselhoModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['conselho/lista-conselho']);
        }

        return $this->render('cadastro-conselho');
    }

    public function actionEditarConselho(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = ConselhoModel::find();
            $conselho = $query->where(['id' => $request->get()])->one();
        }
        
        return $this->render('editar-conselho',[
            'edit' => $conselho
        ]);
    }
    public function actionRealizaEdicaoConselho(){
        $request = \yii::$app->request;
        
        if($request->isPost){
            $model = ConselhoModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            if($model->update()){
                return $this->redirect(['conselho/lista-conselho']);
            }else{
                return $this->redirect(['conselho/lista-conselho', 'msg' => 'erro']);
            }
        }
    }
    public function actionDeletaConselho(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        try {
            $request = \yii::$app->request;
            if($request->isGet){
                $model = ConselhoModel::findOne($request->get('id'));
                if($model->delete()){
                    return $this->redirect(['conselho/lista-conselho',
                        'myAlert' => [
                            'type' => 'success',
                            'msg' => 'Registro Deletado',
                            'redir' => 'index.php?r=conselho/lista-conselho'
                        ]
                    ]);
                }
                
            } 
        }catch (\Throwable $th) {         
            return $this->redirect(['conselho/lista-conselho',
            '   myAlert' => [
                'ty pe' => 'danger',
                'msg    ' => 'Registro não Deletado',
                'redir' => 'index.php?r=conselho/lista-conselho'
                ]
            ]   );
        }
    }
}
?>