<?
namespace app\controllers;

use app\components\alertComponent;
use app\components\legivelComponent;
use app\components\maskComponent;
use app\models\BlocoModel;
use app\models\CondominioModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\MoradorModel;
use app\models\UnidadeModel;
use Yii;

class MoradorController extends Controller{
    public function actionListaMorador(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
    
        $query = (new \yii\db\Query())
        ->select('mor.id,
        mor.idCondominio,
        mor.idBloco,
        mor.idUnidade,
        cond.nomeCondominio,
        b.nomeBloco,
        unid.numero,
        mor.nome,
        mor.cpf,
        mor.email,
        mor.telefone,
        mor.dataCadastro
        ')
        ->from(MoradorModel::tableName().' mor')
        ->innerJoin(CondominioModel::tableName().' cond','cond.id = mor.idCondominio')
        ->innerJoin(BlocoModel::tableName().' b', 'b.id = mor.idBloco')
        ->innerJoin(UnidadeModel::tableName().' unid','unid.id = mor.idUnidade');


        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $morador = $query->orderBy('nome')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();


        return $this->render('lista-morador',[
            'morador'=>$morador,
            'paginacao'=>$paginacao,
        ]);
    }
    public static function listaMoradorSelectEdit($from){
        $query = MoradorModel::find();
        $data = $query->where(['idCondominio' => $from])->orderBy('nome')->all();
        return $data;
    }

    public function actionFiltroMorador(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $request = \yii::$app->request;

        $query = (new \yii\db\Query())
        ->select('mor.id,
        mor.idCondominio,
        mor.idBloco,
        mor.idUnidade,
        cond.nomeCondominio,
        b.nomeBloco,
        unid.numero,
        mor.nome,
        mor.cpf,
        mor.email,
        mor.telefone,
        mor.dataCadastro
        ')
        ->from(MoradorModel::tableName().' mor')
        ->innerJoin(CondominioModel::tableName().' cond','cond.id = mor.idCondominio')
        ->innerJoin(BlocoModel::tableName().' b', 'b.id = mor.idBloco')
        ->innerJoin(UnidadeModel::tableName().' unid','unid.id = mor.idUnidade')
        ->where(['like', 'nome', $request->get('termo1')])
        ->andWhere(['like', 'cond.nomeCondominio', $request->get('termo2')]);


        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $morador = $query->orderBy('nome')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();


        return $this->render('lista-morador',[
            'morador'=>$morador,
            'paginacao'=>$paginacao,
        ]);
    }

    public function actionListaMoradorApi(){
        $request = \yii::$app->request;
        $query = MoradorModel::find();
        $data = $query->where(['idCondominio' => $request->post()])->orderBy('nome')->all();

        $dados = array();
        $i=0;
        foreach($data as $d){
        $dados[$i]['id'] = $d['id'];
        $dados[$i]['nome'] = $d['nome'];
        $i++;
        }
        echo json_encode($dados);

        
    }

    public function actionCadastroMorador(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        return $this->render('cadastro-morador');
    }

    public function actionRealizaCadastroMorador(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new MoradorModel();
            foreach ($request->post() as $ch => $value) {
                if($ch == 'cpf' || $ch == 'telefone'){
                    $getMani[$ch] = maskComponent::desMask($value);
                }else{
                    $getMani[$ch] = $value;
                }
            }
            $model->attributes = $getMani;
            $model->save();
            return $this->redirect(['morador/cadastro-morador']);
        }

        return $this->render('cadastro-morador');
    }

    
    public function actionEditarMorador(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = MoradorModel::find();
            $morador = $query->where(['id' => $request->get()])->one();
        }
        
        return $this->render('editar-morador',[
            'edit' => $morador
        ]);
    }

    public function actionRealizaEdicaoMorador(){
        $request = \yii::$app->request;
        
        if($request->isPost){
            $model = MoradorModel::findOne($request->post('id'));
            foreach ($request->post() as $ch => $value) {
                if($ch == 'cpf' || $ch == 'telefone'){
                    $getMani[$ch] = maskComponent::desMask($value);
                }else{
                    $getMani[$ch] = $value;
                }
            }
            $model->attributes = $getMani;

            if($model->update()){
                return $this->redirect(['morador/lista-morador']);
            }else{
                return $this->redirect(['morador/lista-morador', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaMorador(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        try {
            $request = \yii::$app->request;
            if($request->isGet){
                $model = MoradorModel::findOne($request->get('id'));
                if($model->delete()){
                    return $this->redirect(['morador/lista-morador',
                        'myAlert' => [
                            'type' => 'success',
                            'msg' => 'Registro Deletado',
                            'redir' => 'index.php?r=morador/lista-morador'
                        ]
                    ]);
                }
                
            } 
        }catch (\Throwable $th) {         
            return $this->redirect(['morador/lista-morador',
            'myAlert' => [
                'type' => 'danger',
                'msg' => 'Registro não Deletado',
                'redir' => 'index.php?r=morador/lista-morador'
                ]
            ]   );
        }
    }

    public static function totalmor(){
        $query = \yii::$app->db->createCommand('
        select
        count(id)
        from condominio
        ')->queryScalar();
        return $query;
    }
    
}
?>