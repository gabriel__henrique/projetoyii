<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\User;

class SiteController extends Controller{

  public function actionIndex(){
    if(Yii::$app->user->isGuest){
      return $this->redirect(['site/login']);
    }
    return $this->render('home');
  }

  public function actionLogin(){
    $this->layout = false;
    if (!Yii::$app->user->isGuest) {
      return $this->redirect(['site/home']);
    }

    $request = \yii::$app->request;

    if($request->isPost){

      //$identity = User::findOne(['usuario','senha' => $request->post('usuario','senha')]);
      $identity = User::findOne(['usuario' => $request->post('usuario'), 'senha' => $request->post('senha')]);
      if($identity){
        Yii::$app->user->login($identity);
        return $this->redirect(['index']);
      }else{
        return $this->redirect(['login',
        'myAlert' => [
            'type' => 'warning',
            'msg' => 'Dados de usuario não conferem',
            'redir' => 'site/login'
          ]
        ]);
      }
    }
    return $this->render('login');
  }

  /**
   * logout action.
   * 
   * @return Response
   */
  public function actionLogout(){
    Yii::$app->user->logout();
    return $this->redirect(['site/login']);
  }
}