<?
namespace app\controllers;

use app\components\maskComponent;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\AdministradoraModel;
use Yii;
use yii\db\Command;

class AdministradoraController extends Controller{
    public function actionListaAdministradora(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }


        $query = AdministradoraModel::find();

        $paginacao = new Pagination([
            'defaultPageSize'=> 5,
            'totalCount'=> $query->count(),
        ]);

        $administradora = $query->orderBy('nomeAdm')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();

        return $this->render('lista-administradora', [
            'administradora'=> $administradora,
            'paginacao'=>$paginacao,
        ]);
    }

    public function actionFiltroAdministradora(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $request = \yii::$app->request;

        $query = AdministradoraModel::find()
        ->where(['like', 'nomeAdm', $request->get('termo1')]);

        $paginacao = new Pagination([
            'defaultPageSize'=> 5,
            'totalCount'=> $query->count(),
        ]);

        $administradora = $query->orderBy('nomeAdm')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();

        return $this->render('lista-administradora', [
            'administradora'=> $administradora,
            'paginacao'=>$paginacao,
        ]);
        


        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $unidade = $query->orderBy('numero')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();


        return $this->render('lista-unidade',[
            'unidade'=>$unidade,
            'paginacao'=>$paginacao,
        ]);
    }

    public static function listaAdministradoraSelect(){
        $query = AdministradoraModel::find();

        return $query->orderBy('nomeAdm')->all();
    }

    public function actionCadastroAdministradora(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        return $this->render('cadastro-administradora');
    }

    public function actionRealizaCadastroAdministradora(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new AdministradoraModel();
            foreach ($request->post() as $ch => $value) {
                if($ch == 'cnpj'){
                    $getMani[$ch] = maskComponent::desMask($value);
                }else{
                    $getMani[$ch] = $value;
                }
            }
            $model->attributes = $getMani;
            $model->save();
            return $this->redirect(['administradora/lista-administradora']);
        }

        return $this->render('cadastro-administradora');
    }

    public function actionEditarAdministradora(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = AdministradoraModel::find();
            $administradora = $query->where(['id' => $request->get()])->one();
        }
        
        return $this->render('editar-administradora',[
            'edit' => $administradora
        ]);
    }
    public function actionRealizaEdicaoAdministradora(){
        $request = \yii::$app->request;
        
        if($request->isPost){
            $model = AdministradoraModel::findOne($request->post('id'));
            foreach ($request->post() as $ch => $value) {
                if($ch == 'cnpj'){
                    $getMani[$ch] = maskComponent::desMask($value);
                }else{
                    $getMani[$ch] = $value;
                }
            }
            $model->attributes = $getMani;

            if($model->update()){
                return $this->redirect(['administradora/lista-administradora']);
            }else{
                return $this->redirect(['administradora/lista-administradora', 'msg' => 'erro']);
            }
        }
    }
    public function actionDeletaAdministradora(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        try {
            $request = \yii::$app->request;
            if($request->isGet){
                $model = AdministradoraModel::findOne($request->get('id'));
                if($model->delete()){
                    return $this->redirect(['administradora/lista-administradora',
                        'myAlert' => [
                            'type' => 'success',
                            'msg' => 'Registro Deletado',
                            'redir' => 'index.php?r=administradora/lista-administradora'
                        ]
                    ]);
                }
                
            } 
        }catch (\Throwable $th) {         
            return $this->redirect(['administradora/lista-administradora',
            'myAlert' => [
                'type' => 'danger',
                'msg' => 'Registro não Deletado',
                'redir' => 'index.php?r=administradora/lista-administradora'
                ]
            ]);
        }
    }

    public static function totaladm(){
        $query = \yii::$app->db->createCommand('
        select
        count(id)
        from administradora 
        ')->queryScalar();
        return $query;
    }

    public static function listaContobyAdm(){
        $query = \yii::$app->db->createCommand('
        select
        nomeAdm,
        count(cond.nomeCondominio) as totalcond
        from administradora
        inner join condominio cond on cond.idAdm = administradora.id
        group by nomeAdm
        order by nomeAdm
        ')->queryAll();

        return $query;
    }
}
?>