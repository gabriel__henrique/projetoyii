<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\BlocoModel;
use app\models\CondominioModel;
use Yii;

class BlocoController extends Controller{
    public function actionListaBloco(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }



        $query = (new \yii\db\Query())
        ->select('
        b.id,
        b.idCondominio,
        b.nomeBloco,
        b.qtAndar,
        b.qtAptoAndar,
        cond.nomeCondominio
        ')
        ->from(BlocoModel::tableName().' b')
        ->innerJoin(CondominioModel::tableName().' cond', 'cond.id = b.idCondominio');

        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $bloco = $query->orderBy('nomeBloco')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();


        return $this->render('lista-bloco',[
            'bloco'=>$bloco,
            'paginacao'=>$paginacao,
        ]);
    }

    public function actionFiltroBloco(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $request = \yii::$app->request;

        $query = (new \yii\db\Query())
        ->select('
        b.id,
        b.idCondominio,
        b.nomeBloco,
        b.qtAndar,
        b.qtAptoAndar,
        cond.nomeCondominio
        ')
        ->from(BlocoModel::tableName().' b')
        ->innerJoin(CondominioModel::tableName().' cond','cond.id = b.idCondominio')
        ->where(['like', 'b.nomeBloco', $request->get('termo1')])
        ->andWhere(['like', 'cond.nomeCondominio', $request->get('termo2')]);


        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $bloco = $query->orderBy('nomeBloco')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();


        return $this->render('lista-bloco',[
            'bloco'=>$bloco,
            'paginacao'=>$paginacao,
        ]);
    }

    public static function listaBlocoSelect(){
        $query = BlocoModel::find();
        return $query->orderBy('nomeBloco')->all();
    }

    public static function listaBlocoSelectEdit($from){
        $query = BlocoModel::find();
        $data = $query->where(['idCondominio' => $from])->orderBy('nomeBloco')->all();
        return $data;
    }

    public function actionCadastroBloco(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        return $this->render('cadastro-bloco');
    }

    public function actionRealizaCadastroBloco(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new BlocoModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['bloco/lista-bloco']);
        }

        return $this->render('cadastro-bloco');
    }

    public function actionListaBlocoApi(){
        $request = \yii::$app->request;
        $query = BlocoModel::find();
        $data = $query->where(['idCondominio' => $request->post()])->orderBy('nomeBloco')->all();

        $dados = array();
        $i=0;
        foreach($data as $d){
        $dados[$i]['id'] = $d['id'];
        $dados[$i]['nomeBloco'] = $d['nomeBloco'];
        $i++;
        }
        echo json_encode($dados);

        
    }

    public function actionEditarBloco(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = BlocoModel::find();
            $bloco = $query->where(['id' => $request->get()])->one();
        }
        
        return $this->render('editar-bloco',[
            'edit' => $bloco
        ]);
    }
    public function actionRealizaEdicaoBloco(){
        $request = \yii::$app->request;
        
        if($request->isPost){
            $model = BlocoModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            if($model->update()){
                return $this->redirect(['bloco/lista-bloco']);
            }else{
                return $this->redirect(['bloco/lista-bloco', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaBloco(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        try {
            $request = \yii::$app->request;
            if($request->isGet){
                $model = BlocoModel::findOne($request->get('id'));
                if($model->delete()){
                    return $this->redirect(['bloco/lista-bloco',
                        'myAlert' => [
                            'type' => 'success',
                            'msg' => 'Registro Deletado',
                            'redir' => 'index.php?r=bloco/lista-bloco'
                        ]
                    ]);
                }
                
            } 
        }catch (\Throwable $th) {         
            return $this->redirect(['bloco/lista-bloco',
            'myAlert' => [
                'type' => 'danger',
                'msg' => 'Registro não Deletado',
                'redir' => 'index.php?r=bloco/lista-bloco'
                ]
            ]   );
        }
    }
}
?>
