<?
namespace app\controllers;

use app\models\BlocoModel;
use app\models\CondominioModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\UnidadeModel;
use Yii;

class UnidadeController extends Controller{
    public function actionListaUnidade(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }


        $query = (new \yii\db\Query())
        ->select('unid.id,
        cond.id,
        b.id,
        cond.nomeCondominio,
        b.nomeBloco,
        unid.numero,
        unid.metragem,
        unid.qtVagas
        ')
        ->from(UnidadeModel::tableName().' unid')
        ->innerJoin(CondominioModel::tableName().' cond','cond.id = unid.idCondominio')
        ->innerJoin(BlocoModel::tableName().' b','b.id = unid.idBloco');

        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $unidade = $query->orderBy('numero')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();


        return $this->render('lista-unidade',[
            'unidade'=>$unidade,
            'paginacao'=>$paginacao,
        ]);
    }

    public function actionFiltroUnidade(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $request = \yii::$app->request;

        $query = (new \yii\db\Query())
        ->select('
        unid.id,
        cond.id,
        b.id,
        cond.nomeCondominio,
        b.nomeBloco,
        unid.numero,
        unid.metragem,
        unid.qtVagas
        ')
        ->from(UnidadeModel::tableName().' unid')
        ->innerJoin(CondominioModel::tableName().' cond','cond.id = unid.idCondominio')
        ->innerJoin(BlocoModel::tableName().' b', 'b.id = unid.idBloco')
        ->where(['like', 'unid.numero', $request->get('termo1')])
        ->andWhere(['like', 'cond.nomeCondominio', $request->get('termo2')]);


        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $unidade = $query->orderBy('numero')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();


        return $this->render('lista-unidade',[
            'unidade'=>$unidade,
            'paginacao'=>$paginacao,
        ]);
    }

    public static function listaUnidadeSelect(){
        $query = UnidadeModel::find();

        return $query->orderBy('numero')->all();
    }
    public static function listaUnidadeSelectEdit($from){
        $query = UnidadeModel::find();
        $data = $query->where(['idBloco' => $from])->orderBy('numero')->all();
        return $data;
    }

    public function actionCadastroUnidade(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        return $this->render('cadastro-unidade');
    }

    public function actionRealizaCadastroUnidade(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new UnidadeModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['unidade/cadastro-unidade']);
        }

        return $this->render('cadastro-unidade');
    }
    public function actionListaUnidadeApi(){
        $request = \yii::$app->request;
        $query = UnidadeModel::find();
        $data = $query->where(['idBloco' => $request->post()])->orderBy('numero')->all();

        $dados = array();
        $i=0;
        foreach($data as $d){
        $dados[$i]['id'] = $d['id'];
        $dados[$i]['numero'] = $d['numero'];
        $i++;
        }
        echo json_encode($dados);

        
    }
    public function actionEditarUnidade(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = UnidadeModel::find();
            $unidade = $query->where(['id' => $request->get()])->one();
        }
        
        return $this->render('editar-unidade',[
            'edit' => $unidade
        ]);
    }
    public function actionRealizaEdicaoUnidade(){
        $request = \yii::$app->request;
        
        if($request->isPost){
            $model = UnidadeModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            if($model->update()){
                return $this->redirect(['unidade/lista-unidade']);
            }else{
                return $this->redirect(['unidade/lista-unidade', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaUnidade(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        try {
            $request = \yii::$app->request;
            if($request->isGet){
                $model = BlocoModel::findOne($request->get('id'));
                if($model->delete()){
                    return $this->redirect(['unidade/lista-unidade',
                        'myAlert' => [
                            'type' => 'success',
                            'msg' => 'Registro Deletado',
                            'redir' => 'unidade/lista-unidade'
                        ]
                    ]);
                }
                
            } 
        }catch (\Throwable $th) {         
            return $this->redirect(['unidade/lista-unidade',
            '   myAlert' => [
                'ty pe' => 'danger',
                'msg    ' => 'Registro não Deletado',
                'redir' => 'unidade/lista-unidade'
                ]
            ]   );
        }
    }
}
?>