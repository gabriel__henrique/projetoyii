<?
namespace app\components;

use yii\base\Component;


class filtroComponent extends Component{
    public static function filtro($controller, $action, $termo1, $termo2, $busca, $select){
    $options = "<option value=\"\">Selecione...</option>";
     foreach ($busca as $dados) {
            $options .= "<option value=\"{$dados[$select]}\">{$dados[$select]}</option>";
        }
        $estrutura  = "<form class=\"form-inline my-2 my-lg-0\" id=\"filtro\" method=\"GET\">
        <input type=\"hidden\" name=\"r\" value=\"{$controller}/{$action}\">
        <div class=\"input-group-prepend\">
            <div class=\"input-group-text\">busca por {$termo1}</div>
        </div>
        <input class=\"form-control mr-sm-2 termo1\" type=\"search\" placeholder=\"{$termo1}\" aria-label=\"Buscar\" name=\"termo1\">
        <div class=\"input-group-prepend\">
            <div class=\"input-group-text\">por {$termo2}</div>
        </div>
        <select name=\"termo2\" class=\"termo2 custom-select\">
            {$options}
        </select>
        <button class=\"btn btn-outline-primary ml-5 my-2 my-sm-0\" type=\"submit\">Buscar</button>
        <a class=\"btn btn-outline-danger my-2 my-sm-0 ml-3\" href=\"index.php?r={$controller}/lista-{$controller}\">Limpar</a>
    </form>";
    return $estrutura;
    }
    

    
}
?>